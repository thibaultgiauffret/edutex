import './index.css';
import Ui from './ui';
import { IconAddBorder, IconStretch, IconAddBackground, IconPicture } from '@codexteam/icons';

/**
 * InlineImage Tool for the Editor.js
 * Works images stored in indexedDB
 * Requires no server-side uploader.
 *
 * @typedef {object} InlineImageData
 * @description Tool's input and output data format
 * @property {string} url — image URL
 * @property {string} caption — image caption
 * @property {boolean} withBorder - should image be rendered with border
 * @property {boolean} withBackground - should image be rendered with background
 * @property {boolean} stretched - should image be stretched to full width of container
 */
export default class InlineImage {
  /**
   * Get Tool toolbox settings
   * icon - Tool icon's SVG
   * title - title to show in toolbox
   *
   * @returns {{icon: string, title: string}}
   */
  static get toolbox() {
    return {
      title: 'Image',
      icon: IconPicture,
    };
  }

  /**
   * Render tool`s main Element and fill it with saved data
   *
   * @param {{data: object, config: object, api: object, readOnly: Boolean}}
   *   data — previously saved data
   *   config - custom tool config
   *   api - Editor.js API
   *   readOnly - read-only mode flag
   */
  constructor({
    data, api, config, readOnly,
  }) {
    this.api = api;
    this.readOnly = readOnly;

    this.ui = new Ui({
      data,
      api,
      config,
      readOnly,
      onAddImageData: (imageData) => this.addImageData(imageData),
      onTuneToggled: (tuneName) => this.tuneToggled(tuneName),
    });

    this.data = {
      url: data.url || '',
      caption: data.caption || '',
      name: data.name || '',
      withBorder: data.withBorder !== undefined ? data.withBorder : false,
      withBackground: data.withBackground !== undefined ? data.withBackground : false,
      stretched: data.stretched !== undefined ? data.stretched : false,
    };
  }

  /**
   * Renders Block content
   *
   * @returns {HTMLDivElement}
   */
  render() {
    return this.ui.render(this.data);
  }

  /**
   * Returns Block data
   *
   * @returns {InlineImageData}
   */
  save() {
    const { caption } = this.ui.nodes;

    this.data.caption = caption.innerHTML;
    return this.data;
  }

  /**
   * Validate saved data returned by the save method
   *
   * @param {object} savedData Block saved data
   */
  validate(savedData) {
    // if (!savedData.url.trim()) {
    //   return false;
    // }
    return true;
  }

  /**
   * Sanitizer rules
   *
   * @see {@link https://editorjs.io/sanitizer}
   */
  static get sanitize() {
    return {
      url: {},
      name: {},
      withBorder: {},
      withBackground: {},
      stretched: {},
      caption: {
        br: true,
      },
    };
  }

  /**
   * Read pasted image and convert it to base64
   *
   * @param {File} file Image file
   * @returns {Promise<InlineImageData>}
   */
  onDropHandler(file) {
    const reader = new FileReader();

    reader.readAsDataURL(file);

    return new Promise((resolve) => {
      reader.onload = (event) => {
        resolve({
          url: event.target.result,
          caption: file.name,
        });
      };
    });
  }

  /**
   * On paste callback that is fired from Editor.
   *
   * @param {PasteEvent} event - event with pasted config
   */
  onPaste(event) {
    this.ui.showLoader();
    switch (event.type) {
      case 'tag':
        this.data = {
          url: event.detail.data.src,
        };
        break;
      case 'pattern':
        this.data = {
          url: event.detail.data,
        };
        break;
      case 'file':
        this.onDropHandler(event.detail.file)
          .then((data) => {
            this.data = data;
          });
        break;
      default:
        break;
    }
  }

  /**
   * Callback for updating data when the image is embedded
   *
   * @param {object} imageData Image data
   */
  addImageData(imageData) {
    this.data = imageData;
  }

  /**
   * Returns image data
   *
   * @return {InlineImageData}
   */
  get data() {
    return this._data;
  }

  /**
   * Set image data and update the view
   *
   * @param {InlineImageData} data Image data
   */
  set data(data) {
    this._data = { ...this.data, ...data };
    if (this._data.url != "") {
      this._data.url = "./examples/placeholder.png";

      // If data.url begins with "blob:" then it is an image from the database
      this.getBlobUrl(this.data.name).then((blobUrl) => {
        this.data.url = blobUrl;
        console.log(this.data.url);
        if (this.ui.nodes.image) {
          this.ui.nodes.image.src = this.data.url;
        }

        if (this.ui.nodes.caption) {
          this.ui.nodes.caption.innerHTML = this.data.caption;
        }
      });
    }

  }

  getImageFromDB(name) {
    return new Promise((resolve, reject) => {
      // Get the project name
      const project = localStorage.getItem('currentProject');
      // Open the indexedDB database
      const request = indexedDB.open("edutex_db_" + project, 1);
      // Read the data from the store "edutex_assets"
      request.onsuccess = function (event) {
        const db = event.target.result;
        const transaction = db.transaction(["edutex_assets"], "readwrite");
        const objectStore = transaction.objectStore("edutex_assets");
        // Find the image in the store with the name
        const requestImage = objectStore.get(name);
        requestImage.onsuccess = function (event) {
          if (requestImage.result == undefined) {
            console.log("No data found");
          }
          else {
            resolve(requestImage.result);
          };
        }
      }
    }).catch((error) => {
      console.log(error);
    })
  }

  getBlobUrl(name) {
    return new Promise((resolve, reject) => {
      this.getImageFromDB(name).then((image) => {
        console.log(image);
        // Get the data in image.data and create a blob with it
        const blob = new Blob([image.data], { type: image.type });
        // Create a URL for the blob
        const url = URL.createObjectURL(blob);
        resolve(url);
      });
    });
  }

  /**
   * Specify paste substitutes
   *
   * @see {@link https://editorjs.io/paste-substitutions}
   */
  static get pasteConfig() {
    return {
      patterns: {
        image: /https?:\/\/\S+\.(gif|jpe?g|tiff|png)$/i,
      },
      tags: ['img'],
      files: {
        mimeTypes: ['image/*'],
      },
    };
  }

  /**
   * Makes buttons with tunes: add background, add border, stretch image
   *
   * @return {HTMLDivElement}
   */
  renderSettings() {
    return this.ui.renderSettings(this.data);
  }

  /**
   * Callback fired when Block Tune is activated
   *
   * @param {string} tuneName - tune that has been clicked
   * @returns {void}
   */
  tuneToggled(tuneName) {
    const value = !this.data[tuneName];
    this.data = { [tuneName]: value };
    this.ui.applyTune(tuneName, value);
  }

  /**
   * Notify core that read-only mode is supported
   *
   * @returns {boolean}
   */
  static get isReadOnlySupported() {
    return true;
  }
}
