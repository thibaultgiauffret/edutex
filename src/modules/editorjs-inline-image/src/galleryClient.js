/**
 * Client for image gallery (indexedDB)
 */
export default class galleryClient {
  constructor(config) {
  }

  /**
   * Search images
   */
  searchImages(callback) {
    this.getImagesInDB().then((images) => {
      // If there are no images in the database, return an empty array
      if (images.length == 0) {
        callback([]);
      }
      callback(this.parseResponse(images));
    });

  }

  getImagesInDB() {
    return new Promise((resolve, reject) => {
      // Get the current project in local storage
      const project = localStorage.getItem('currentProject');
      // Open the indexedDB database
      const request = indexedDB.open("edutex_db_" + project, 1);
      // Read the data from the store "edutex_assets"
      request.onsuccess = function (event) {
        const db = event.target.result;
        const transaction = db.transaction(["edutex_assets"], "readwrite");
        const objectStore = transaction.objectStore("edutex_assets");
        // Get all the data from the store
        const requestImage = objectStore.getAll();
        requestImage.onsuccess = function (event) {
            // For each image in the store, check if its type starts with "image/"
            // If it is, add it to the array of images
            var images = [];
            requestImage.result.forEach(function (image) {
              if (image.type.indexOf("image/") === 0) {
                images.push(image);
              }
            });
            resolve(images);
        }
      }
    }).catch((error) => {
      console.log(error);
    });
  }


  /**
   * Parses the response 
   */
  parseResponse(results) {
    return results.map((image) => this.buildImageObject(image));
  }

  /**
   * Builds an image object
   */
  buildImageObject(image) {
    // Get the data in image.data and create a blob with it
    const blob = new Blob([image.data], { type: image.type });
    // Create a URL for the blob
    const url = URL.createObjectURL(blob);

    return {
      url: url,
      thumb: url,
      name: image.name,
    };
  }
}
