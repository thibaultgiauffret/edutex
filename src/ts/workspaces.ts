export let default_workspace: string = `
<div class="full_height row">
    <div class="sidebyside col-12 col-xs-12 col-sm-12 col-md-6" id="left">
        <div class="card editor-card" id="editor-card">
            <div class="card-header">
                <i class="fa-solid fa-fw fa-pen-to-square"></i>&nbsp;Éditeur LaTeX
                <div class="float-end d-flex align-items-center">
                    <span class="nav-item dropdown d-inline-block mx-1" data-bs-toggle="tooltip"
                        data-bs-title="Voir l'historique">
                        <a class="nav-link dropdown-toggle text-secondary" type="button" id="historyBtn" role="button"
                            data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="fa-solid fa-fw fa-history"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-end" id="historyList">
                        </ul>
                    </span>
                    <button type="button" class="nav-link text-secondary  d-inline-block mx-2" id="clearBtn"
                        data-bs-toggle="tooltip" data-bs-title="Effacer l'éditeur">
                        <i class="fa-solid fa-fw fa-trash"></i>
                    </button>
                    <button type="button" class="nav-link text-secondary  d-inline-block mx-2" id="downloadTeXBtn"
                        data-bs-toggle="tooltip" data-bs-title="Télécharger le fichier TeX">
                        <i class="fa-solid fa-fw fa-download"></i>
                    </button>
                    <button type="button" class="nav-link text-secondary  d-inline-block mx-2" id="saveBtn"
                        data-bs-toggle="tooltip" data-bs-title="Enregistrer (navigateur)">
                        <i class="fa-solid fa-fw fa-save"></i>
                    </button>
                    <button type="button" class="btn btn-sm btn-success d-inline-block ms-2" id="compileBtn"
                        data-bs-toggle="tooltip" data-bs-title="Compiler" disabled>Initialisation</button>
                    </ul>
                </div>
            </div>
            <div class="card-body" id="editor-card-body">
            </div>
            <div class="card-footer">
                <button class="btn btn-link text-secondary" disabled><i class="fa-solid fa-fw fa-wrench"></i></button>

                <!-- Button to add a table -->
                <button type="button" class="btn btn-sm btn-primary" id="addTableBtn" data-bs-toggle="tooltip"
                    data-bs-title="Ajouter un tableau">
                    <i class="fa-solid fa-fw fa-table"></i>
                </button>

                <!-- Button to add a figure -->
                <button type="button" class="btn btn-sm btn-primary" id="addFigureBtn" data-bs-toggle="tooltip"
                    data-bs-title="Ajouter une figure">
                    <i class="fa-solid fa-fw fa-image"></i>
                </button>

                <span class="float-end">
                    <!-- Select box to choose the file to edit -->
                    <select class="form-select form-select-sm" id="fileSelect">
                    </select>
            </div>
        </div>

    </div>
    <div class="sidebyside col-12 col-xs-12 col-sm-12 col-md-6" id="right">
        <div class="card" id="pdf-card">
            <div class="card-header">
                <i class="fa-solid fa-fw fa-file-pdf"></i>&nbsp;Rendu PDF
                <div class="float-end d-flex align-items-center">
                    <span id="compile-time" class="d-none text-secondary"></span>
                    <button type="button" class="btn btn-sm btn-danger d-inline-block ms-2" id="downloadPDFBtn"
                        data-bs-toggle="tooltip" data-bs-title="Télécharger le PDF">
                        <i class="fa-solid fa-fw fa-download"></i>
                    </button>
                </div>
            </div>
            <div class="card-body" id="pdf-card-body">
                <div id="pdfbox">
                    <div id="pdf-placeholder">
                        <p><i class="fa-solid fa-fw fa-file-pdf fa-4x"></i></p>
                        <p>Pas de PDF généré pour l'instant...</p>
                    </div>
                </div>
            </div>
        </div>


        <div class="card" id="console-card">
            <div class="card-header">
                <i class="fa-solid fa-fw fa-terminal"></i>&nbsp;Console
            </div>
            <div class="card-body" id="console-card-body">
                <pre id="console"></pre>
            </div>
        </div>

    </div>
</div>
`;

export let simplified_workspace: string = `
<div class="row full_height">
<div class="col-12 col-xs-12 col-sm-12 col-md-6 sidebyside" id="left">
    <div class="card mb-3 editor-card h-100" id="simple-editor-card">
        <div class="card-header">
            <i class="fa-solid fa-fw fa-pen-to-square"></i>&nbsp;Éditeur simplifié
        </div>
        <div class="card-body ps-5" id="WYSIWYGEditorContainer">
            <div id="WYSIWYGEditor"></div>
        </div>
        <div class="card-footer">
        </div>
    </div>
</div>



<div class="col-12 col-xs-12 col-sm-12 col-md-6 sidebyside" id="right" style="max-height:100vh;">


    <div class="card editor-card" id="editor-card">
        <div class="card-header">
            <i class="fa-solid fa-fw fa-pen-to-square"></i>&nbsp;Éditeur LaTeX
            <div class="float-end d-flex align-items-center">
                <span class="nav-item dropdown d-inline-block mx-1" data-bs-toggle="tooltip"
                    data-bs-title="Voir l'historique">
                    <a class="nav-link dropdown-toggle text-secondary" type="button" id="historyBtn"
                        role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="fa-solid fa-fw fa-history"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-end" id="historyList">
                    </ul>
                </span>
                <button type="button" class="nav-link text-secondary  d-inline-block mx-2" id="clearBtn"
                    data-bs-toggle="tooltip" data-bs-title="Effacer l'éditeur">
                    <i class="fa-solid fa-fw fa-trash"></i>
                </button>
                <button type="button" class="nav-link text-secondary  d-inline-block mx-2" id="downloadTeXBtn"
                    data-bs-toggle="tooltip" data-bs-title="Télécharger le fichier TeX">
                    <i class="fa-solid fa-fw fa-download"></i>
                </button>
                <button type="button" class="nav-link text-secondary  d-inline-block mx-2" id="saveBtn"
                    data-bs-toggle="tooltip" data-bs-title="Enregistrer (navigateur)">
                    <i class="fa-solid fa-fw fa-save"></i>
                </button>
                <button type="button" class="btn btn-sm btn-success d-inline-block ms-2" id="compileBtn"
                    data-bs-toggle="tooltip" data-bs-title="Compiler" disabled>Initialisation</button>
                </ul>
                <button type="button" class="btn btn-sm btn-primary d-inline-block ms-2" id="resultsBtn"
                    data-bs-toggle="tooltip" data-bs-title="Voir le résultats">
                    <i class="fa-solid fa-fw fa-file-pdf"></i>
                </button>
                </ul>
            </div>
        </div>
        <div class="card-body" id="editor-card-body">
        </div>
        <div class="card-footer" id="editor-card-footer">

            <button class="btn btn-link text-secondary" disabled><i
                    class="fa-solid fa-fw fa-wrench"></i></button>

            <!-- Button to add a table -->
            <button type="button" class="btn btn-sm btn-primary" id="addTableBtn" data-bs-toggle="tooltip"
                data-bs-title="Ajouter un tableau">
                <i class="fa-solid fa-fw fa-table"></i>
            </button>

            <!-- Button to add a figure -->
            <button type="button" class="btn btn-sm btn-primary" id="addFigureBtn" data-bs-toggle="tooltip"
                data-bs-title="Ajouter une figure">
                <i class="fa-solid fa-fw fa-image"></i>
            </button>

            <span class="float-end">
                <!-- Select box to choose the file to edit -->
                <select class="form-select form-select-sm" id="fileSelect">
                </select>
        </div>
    </div>

</div>
</div>

<!-- Render Modal -->
<div class="modal fade" id="resultsModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Résultats</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="card" id="pdf-card" style="height:70vh;">
                    <div class="card-header">
                        <i class="fa-solid fa-fw fa-file-pdf"></i>&nbsp;Rendu PDF
                        <div class="float-end d-flex align-items-center">
                            <span id="compile-time" class="d-none text-secondary"></span>
                            <button type="button" class="btn btn-sm btn-danger d-inline-block ms-2"
                                id="downloadPDFBtn" data-bs-toggle="tooltip" data-bs-title="Télécharger le PDF">
                                <i class="fa-solid fa-fw fa-download"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body" id="pdf-card-body">
                        <div id="pdfbox">
                            <div id="pdf-placeholder">
                                <p><i class="fa-solid fa-fw fa-file-pdf fa-4x"></i></p>
                                <p>Pas de PDF généré pour l'instant...</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" id="console-card">
                    <div class="card-header">
                        <i class="fa-solid fa-fw fa-terminal"></i>&nbsp;Console
                    </div>
                    <div class="card-body" id="console-card-body">
                        <pre id="console"></pre>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>



`;