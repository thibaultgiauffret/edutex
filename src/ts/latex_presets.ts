export let table_preset = `
\\begin{table}[h!]
    \\centering
    \\begin{tabular}{|c|c|c|}
        \\hline
        \\textbf{Colonne 1} & \\textbf{Colonne 2} & \\textbf{Colonne 3} \\\\ 
        \\hline
        \\textbf{Ligne 1} & & \\\\
        \\hline
        \\textbf{Ligne 2} & & \\\\ 
        \\hline
        \\textbf{Ligne 3} & & \\\\
        \\hline
    \\end{tabular}
    \\caption{Titre du tableau}
    \\label{tab:my_label}
\\end{table}\n`;

export let figure_preset = `
\\begin{figure}[h!]
    \\centering
    \\includegraphics[width=0.5\\textwidth]{logo.png}
    \\caption{Titre de la figure}
    \\label{fig:my_label}
\\end{figure}\n`;