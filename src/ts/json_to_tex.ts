// Translate the JSON from the simplified editor into TeX
// Many optimizations can be made here...

export default class JSONTeXTranslator {
    private _packages: string;
    public _blocks: any[];

    constructor() {
        this._packages = "";
        this._blocks = [];
    }

    // This function translates the content of the simplified editor (saved in JSON) into TeX
    public async translate(json: any, column: boolean = false) {
        // return new Promise((resolve, reject) => {
        // Get the blocks from the JSON
        let blocks = json.blocks;
        // Initialize the TeX string
        let result = "";
        this._blocks = [];
        this._packages = "";
        // Iterate over the blocks
        for (let i = 0; i < blocks.length; i++) {
            // Get the block
            let block = blocks[i];
            // Get the id of the block
            let id = block.id;
            // Get the type of the block
            let type = block.type;
            // Get the data of the block
            let data = block.data;
            // Get the text of the block
            let text = data.text;
            // Create a block object
            let blockTex = {id: id, tex: ""};
            // Check the type of the block
            switch (type) {
                case "header":
                    // Get the level of the header
                    let level = data.level;
                    blockTex.tex += "\\";
                    // Add the TeX code for the header to the TeX string
                    for (let j = 1; j < level; j++) {
                        blockTex.tex += "sub";
                    }
                    blockTex.tex += "section{" + text + "}\n";
                    break;
                case "paragraph":
                    // Replace <b> tags with \textbf{}
                    text = text.replace(/<b>/g, "\\textbf{");
                    text = text.replace(/<\/b>/g, "}");
                    // Replace <i> tags with \textit{}
                    text = text.replace(/<i>/g, "\\textit{");
                    text = text.replace(/<\/i>/g, "}");
                    // Replace <u> tags with \underline{}
                    text = text.replace(/<u class="cdx-underline">/g, "\\underline{");
                    text = text.replace(/<u>/g, "\\underline{");
                    text = text.replace(/<\/u>/g, "}");
                    // Replace <br> tags with \newline
                    text = text.replace(/<br>/g, "");
                    // Replace <a> tags with \href{}
                    // Detect if link is present
                    let link = text.match(/<a href="/);
                    if (link != null) {
                        this._packages += this.addPackages("\\usepackage{hyperref}");
                    }
                    text = text.replace(/<a href="/g, "\\href{");
                    text = text.replace(/">/g, "}{");
                    text = text.replace(/<\/a>/g, "}");
                    // Add the TeX code for the paragraph to the TeX string
                    blockTex.tex += text + "\n";
                    break;
                case "list":
                    // Get the style of the list
                    let style = data.style;
                    // Get the items of the list
                    let items = data.items;
                    // Add the TeX code for the list to the TeX string
                    blockTex.tex += "\n\\begin{";
                    if (style === "ordered") {
                        blockTex.tex += "enumerate";
                    } else {
                        blockTex.tex += "itemize";
                    }
                    blockTex.tex += "}\n";
                    for (let j = 0; j < items.length; j++) {
                        // Remove <br> tags from the items
                        items[j] = items[j].replace(/<br>/g, "");
                        blockTex.tex += "\\item " + items[j] + "\n";
                    }
                    blockTex.tex += "\\end{";
                    if (style === "ordered") {
                        blockTex.tex += "enumerate";
                    } else {
                        blockTex.tex += "itemize";
                    }
                    blockTex.tex += "}\n";
                    break;
                case "image":
                    if (data.name != "" && data.name != null && data.name != undefined) {
                        if (!column) {
                            let name = data.name;
                            // Add the packages for the image
                            this._packages += this.addPackages("\\usepackage{graphicx}");
                            this._packages += this.addPackages("\\usepackage{float}");
                            // Get the caption of the image
                            let caption = data.caption;
                            // Remove <br> tags from the caption
                            caption = caption.replace(/<br>/g, "");
                            // Add the TeX code for the image to the TeX string
                            blockTex.tex += "\\begin{figure}[h!]\n";
                            blockTex.tex += "\\centering\n";
                            blockTex.tex += "\\includegraphics[width=\\textwidth]{" + name + "}\n";
                            blockTex.tex += "\\caption{" + caption + "}\n";
                            blockTex.tex += "\\end{figure}\n";
                        } else {
                            let name = data.name;
                            // Add the packages for the image
                            this._packages += this.addPackages("\\usepackage{graphicx}");
                            this._packages += this.addPackages("\\usepackage{float}");
                            this._packages += this.addPackages("\\usepackage{caption}");
                            // Get the caption of the image
                            let caption = data.caption;
                            // Remove <br> tags from the caption
                            caption = caption.replace(/<br>/g, "");
                            // Add the TeX code for the image to the TeX string
                            blockTex.tex += "\\begin{center}\n";
                            blockTex.tex += "\\includegraphics[width=\\textwidth]{" + name + "}\n";
                            blockTex.tex += "\\captionof{figure}{" + caption + "}\n";
                            blockTex.tex += "\\end{center}\n";
                        }
                    }
                    break;
                case "table":
                    this._packages += this.addPackages("\\usepackage{float}");
                    this._packages += this.addPackages("\\usepackage{tabularx}");
                    // Get the data of the table
                    let tableData = data.content;
                    // Get the number of columns of the table
                    let columns = tableData[0].length;
                    // Add the TeX code for the table to the TeX string
                    blockTex.tex += "\\begin{table}[h!]\n";
                    blockTex.tex += "\\centering\n";
                    blockTex.tex += "\\begin{tabular}{";
                    for (let j = 0; j < columns; j++) {
                        blockTex.tex += "|c";
                    }
                    blockTex.tex += "|}\n";
                    blockTex.tex += "\\hline\n";
                    for (let j = 0; j < tableData.length; j++) {
                        for (let k = 0; k < tableData[j].length; k++) {
                            // Remove <br> tags from the table data
                            tableData[j][k] = tableData[j][k].replace(/<br>/g, "");
                            blockTex.tex += tableData[j][k];
                            if (k !== tableData[j].length - 1) {
                                blockTex.tex += " & ";
                            }
                        }
                        blockTex.tex += "\\\\ \\hline\n";
                    }
                    blockTex.tex += "\\end{tabular}\n";
                    blockTex.tex += "\\end{table}\n";
                    break;
                case "columns":
                    // Get the data of the columns
                    let columnsData = data.cols;
                    // Get the number of columns
                    let numColumns = columnsData.length;
                    let columnWidth = Math.round(1 / numColumns * 100) / 100;
                    // For each column, create a minipage with the width of the column
                    blockTex.tex += "\n";
                    for (let j = 0; j < numColumns; j++) {
                        let column = columnsData[j];
                        let colTex = "";
                        await this.translate(column, true).then((columnResult) => {
                            console.log(columnResult);
                            colTex += "\\begin{minipage}{" + columnWidth + "\\textwidth}\n";
                            colTex += columnResult;
                            colTex += "\\end{minipage}\n";
                            // Add the TeX code for the columns to the TeX string
                            blockTex.tex += colTex;
                        });
                    }
                    break;

            }
            // Add the block to the blocks array
            this._blocks.push(blockTex);
        }
        if (!column) {
            result = "\\documentclass{article}\n\n";
            result += this._packages;
            result += "\n\\begin{document}\n\n";
            result += this.assembleTeX();
            result += "\n\\end{document}"
            return result;
        } else {
            return this.assembleTeX(false);
        }
        // });
    }

    private addPackages(pack: string) {
        // TODO : improve this function
        // Check if the package is already in the packages string
        if (this._packages.indexOf(pack) === -1) {
            // Add the package to the packages string
            return pack + "\n"
        }
        return ""
    };

    private assembleTeX(emptyLines: boolean = true) {
        let tex = "";
        // For each block, add the TeX code to the TeX string
        for (let i = 0; i < this._blocks.length; i++) {
            tex += this._blocks[i].tex;
            if (emptyLines) {
                tex += "\n";
            }
        }
        return tex;
    }

    public getBlockTex(id: string) {
        // Get the block from the blocks array
        let block = this._blocks.find(block => block.id === id);
        // Return the TeX code of the block
        return block.tex;
    }
}