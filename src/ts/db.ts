export default class DB {

    private db: IDBDatabase | null = null;

    public init(name: string) {

        return new Promise<void>((resolve, reject) => {
            // Open a database
            const openRequest = window.indexedDB.open("edutex_db_" + name, 1);

            openRequest.addEventListener("upgradeneeded", (event) => {
                // Grab a reference to the opened database
                this.db = (event.target as IDBOpenDBRequest).result;

                // Create stores in the database
                const editorStore = this.db.createObjectStore("edutex_tex", {
                    keyPath: "id",
                    autoIncrement: true,
                });
                const assetStore = this.db.createObjectStore("edutex_assets", {
                    keyPath: "name",
                });
                const pdfStore = this.db.createObjectStore("edutex_pdf", {
                    keyPath: "id",
                    autoIncrement: true,
                });

                // Define what data items the stores will contain
                editorStore.createIndex("timestamp", "timestamp");
                editorStore.createIndex("content", "content");
                editorStore.createIndex("simpleContent", "simpleContent");
                assetStore.createIndex("name", "name", { unique: true });
                assetStore.createIndex("type", "type");
                assetStore.createIndex("size", "size");
                assetStore.createIndex("data", "data");
                pdfStore.createIndex("file", "file");

                console.log("Database setup complete");
            });

            openRequest.addEventListener("success", (event) => {
                console.log("Database opened successfully");
                // Store the opened database object in the db variable
                if (!openRequest) {
                    throw new Error("Open request is not initialized");
                }
                this.db = openRequest.result;
                resolve();
            });

            openRequest.addEventListener("error", (event) => {
                console.log("Database error: " + event);
                reject();
            });
        });
    }

    // Delete the database with the given name
    public deleteDatabase(name: string) {
        return new Promise<void>((resolve, reject) => {

            //    Delete the database
            const deleteRequest = window.indexedDB.deleteDatabase("edutex_db_" + name);

            // Success handler
            deleteRequest.addEventListener("success", () => {
                console.log("Database deleted successfully");
                resolve();
            });

            // Error handler
            deleteRequest.addEventListener("error", () => {
                console.error("Database not deleted due to error");
                reject();
            });

        });
    }

    // Add the editor content to the database
    public saveEditorContent(timestamp: string, content: string, simpleContent: string = "") {
        if (!this.db) {
            throw new Error("Database is not initialized");
        }
        let transaction = this.db.transaction(["edutex_tex"], "readwrite");
        let objectStore = transaction.objectStore("edutex_tex");

        // Check the number of entries in the object store
        const request = objectStore.getAll();

        // if there are more than 10 entries, remove the oldest one
        request.addEventListener("success", () => {
            if (request.result.length >= 10) {
                objectStore.delete(request.result[0].id);
            }
        });

        // Add the new item to the object store
        objectStore.add({ timestamp: timestamp, content: content, simpleContent: simpleContent });

        // Success handler
        transaction.addEventListener("complete", () => {
            console.log("Editor content saved");
        });

        // Error handler
        transaction.addEventListener("error", () => {
            console.error("Transaction not opened due to error");
        });
    }

    // Get the last 10 entries from the object store "edutex_tex"
    public getLastTenEditorContent() {
        return new Promise((resolve, reject) => {

            if (!this.db) {
                throw new Error("Database is not initialized");
            }
            const transaction = this.db.transaction(["edutex_tex"], "readonly");
            const objectStore = transaction.objectStore("edutex_tex");

            // Get all data from the object store
            const request = objectStore.getAll();

            // Success handler
            request.addEventListener("success", () => {

                // If there is no data, resolve with null
                if (request.result.length == 0) {
                    console.log("Last 10 entries not found");
                    resolve(null);
                }
                // Get the last 10 entries
                let lastTenEntries = request.result.slice(-10);
                resolve(lastTenEntries);
            });

            // Error handler
            request.addEventListener("error", () => {
                console.error("An error occurred");
                reject();
            });
        });
    }

    // Get the last entry from the object store
    public getLastEditorContent() {
        return new Promise((resolve, reject) => {

            if (!this.db) {
                throw new Error("Database is not initialized");
            }
            const transaction = this.db.transaction(["edutex_tex"], "readonly");
            const objectStore = transaction.objectStore("edutex_tex");
            const request = objectStore.getAll();

            // Success handler
            request.addEventListener("success", () => {

                // If there is no data, resolve with null
                if (request.result.length == 0) {
                    console.log("Last entry not found");
                    resolve(null);
                } else {
                    resolve(request.result[request.result.length - 1]);
                }
            });

            // Error handler
            request.addEventListener("error", () => {
                console.error("An error occurred");
                reject();
            });

        });
    }

    // Get the editor content from the object store with the given timestamp
    public getEditorContentByTimestamp(timestamp: string) {
        return new Promise((resolve, reject) => {

            if (!this.db) {
                throw new Error("Database is not initialized");
            }
            const transaction = this.db.transaction(["edutex_tex"], "readonly");
            const objectStore = transaction.objectStore("edutex_tex");

            // Get all data from the object store
            const request = objectStore.getAll();

            // Success handler
            request.addEventListener("success", () => {

                // If there is no data, resolve with null
                if (request.result.length == 0) {
                    console.log("No data found for timestamp " + timestamp);
                    resolve(null);
                }

                // Get the last data from the object store
                let result = request.result.find((element) => element.timestamp == timestamp);
                resolve(result);
            });

            // Error handler
            request.addEventListener("error", () => {
                console.error("An error occurred");
                reject();
            });

        });
    }

    // Remove the last entry from the object store
    public removeLastEditorContent() {

        if (!this.db) {
            throw new Error("Database is not initialized");
        }
        const transaction = this.db.transaction(["edutex_tex"], "readwrite");
        const objectStore = transaction.objectStore("edutex_tex");

        // Get all data from the object store
        const request = objectStore.getAll();

        // Success handler
        request.addEventListener("success", () => {

            // If there is no data, resolve with null
            if (request.result.length == 0) {
                console.log("No data found, nothing to delete");
                return null;
            }

            // Get the last data from the object store
            objectStore.delete(request.result[request.result.length - 1].id);
        });

        // Error handler
        request.addEventListener("error", () => {
            console.error("An error occurred");
        });
    }

    // Remove the entry from the object store with the given timestamp
    public removeEditorContentByTimestamp(timestamp: string) {
        return new Promise<void>((resolve, reject) => {

            if (!this.db) {
                throw new Error("Database is not initialized");
            }
            const transaction = this.db.transaction(["edutex_tex"], "readwrite");
            const objectStore = transaction.objectStore("edutex_tex");

            // Get all data from the object store
            const request = objectStore.getAll();

            // Success handler
            request.addEventListener("success", () => {

                // If there is no data, resolve with null
                if (request.result.length == 0) {
                    console.log("No data found, nothing to delete");
                    resolve();
                }

                // Get the entry from the object store with the given timestamp
                let result = request.result.find((element) => element.timestamp == timestamp);

                // Delete the entry from the object store
                if (result == undefined) {
                    console.log("No data found for timestamp " + timestamp);
                    reject();
                }
                objectStore.delete(result.id);
                resolve();
            });

            // Error handler
            request.addEventListener("error", () => {
                console.error("An error occurred");
                reject();
            });
        });
    }


    // Add asset data to the database
    public addAssetData(file: File, force: boolean = false) {
        return new Promise((resolve, reject) => {
            // Check if the file already exists
            this.fileExists(file.name).then((exists) => {
                if (!this.db) {
                    throw new Error("Database is not initialized");
                }
                const transaction = this.db.transaction(["edutex_assets"], "readwrite");
                const objectStore = transaction.objectStore("edutex_assets");

                if (exists && !force) {
                    if (confirm("Le fichier " + file.name + " existe déjà. Voulez-vous le remplacer ?")) {
                        const objectStore = transaction.objectStore("edutex_assets");
                        objectStore.put(file);
                        resolve("replaced");
                    } else {
                        resolve("not_added");
                    }
                } else {
                    objectStore.add(file);
                    resolve("added");
                }


                // Success handler
                transaction.addEventListener("complete", () => {
                    console.log("Transaction completed");
                });

                // Error handler
                transaction.addEventListener("error", () => {
                    console.error("Transaction not opened due to error");
                });
            }).catch((error) => {
                console.error(error);
                reject();
            });
        });

    }

    private fileExists(filename: string) {
        return new Promise<boolean>((resolve, reject) => {
            if (!this.db) {
                throw new Error("Database is not initialized");
            }
            const transaction = this.db.transaction(["edutex_assets"], "readonly");
            const objectStore = transaction.objectStore("edutex_assets");

            // Get the data from the objectStore with the given filename
            const request = objectStore.get(filename);

            // Success handler
            request.addEventListener("success", () => {
                if (request.result) {
                    resolve(true);
                } else {
                    resolve(false);
                }
            });

            // Error handler
            request.addEventListener("error", () => {
                console.error("An error occurred");
                reject();
            });
        });
    }

    // Add tex file to the database
    public addTexFile(file: any) {
        return new Promise((resolve, reject) => {

            let content = file.data;
            // Convert the content to string
            if (typeof content != "string") {
                content = new TextDecoder("utf-8").decode(content);
            }
            if (!this.db) {
                throw new Error("Database is not initialized");
            }
            const transaction = this.db.transaction(["edutex_tex"], "readwrite");
            const objectStore = transaction.objectStore("edutex_tex");
            objectStore.add({ timestamp: new Date().toISOString(), content: content, simpleContent: "" });

            // Success handler
            transaction.addEventListener("complete", () => {
                console.log("Transaction completed");

                resolve(content);
            });

            // Error handler
            transaction.addEventListener("error", () => {
                console.error("Transaction not opened due to error");
                reject();
            })
        });
    }
    // Save image data from URL to the database
    public saveImage(url: string) {
        return new Promise<void>((resolve, reject) => {
            console.log("Saving image " + url)
            // Download the image
            let xhr = new XMLHttpRequest();
            xhr.open('GET', url, true);
            // Allow CORS
            xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
            xhr.responseType = 'arraybuffer';
            xhr.onload = (e) => {
                if (xhr.status == 200) {
                    // Get the image data
                    let arrayBuffer = xhr.response;
                    if (arrayBuffer) {
                        let byteArray = new Uint8Array(arrayBuffer);
                        let blob = new Blob([byteArray], { type: "application/octet-stream" });
                        // Save the image data to the database
                        this.addAssetData(new File([blob], url.split('/').pop() as string)).then(() => {
                            resolve();
                        }).catch((error) => {
                            reject(error);
                        })

                    } else {
                        reject("Array buffer is null");
                    }
                }
            };
            xhr.send();
        });
    }

    // Update asset data in the database
    public updateAssetData(file: File) {
        return Promise.resolve().then(() => {
            if (!this.db) {
                throw new Error("Database is not initialized");
            }
            // Store the file in the database
            const transaction = this.db.transaction(["edutex_assets"], "readwrite");
            const objectStore = transaction.objectStore("edutex_assets");
            objectStore.put(file);

            // Success handler
            transaction.addEventListener("complete", () => {
                console.log("Transaction completed");
            });

            // Error handler
            transaction.addEventListener("error", () => {
                console.error("Transaction not opened due to error");
            });
        });
    }

    // Get all the asset data from the database
    public getAssetsData() {
        return new Promise((resolve, reject) => {
            if (!this.db) {
                throw new Error("Database is not initialized");
            }
            const transaction = this.db.transaction(["edutex_assets"], "readonly");
            const objectStore = transaction.objectStore("edutex_assets");
            const request = objectStore.getAll();

            // Success handler
            request.addEventListener("success", () => {
                if (request.result.length == 0) {
                    console.log("No data found");
                    resolve(null);
                }
                resolve(request.result);
            });

            // Error handler
            request.addEventListener("error", () => {
                console.error("An error occurred");
                reject();
            });
        });
    }

    public getAssetData(filename: string) {
        return new Promise((resolve, reject) => {
            if (!this.db) {
                throw new Error("Database is not initialized");
            }
            const transaction = this.db.transaction(["edutex_assets"], "readonly");
            const objectStore = transaction.objectStore("edutex_assets");

            // Get the data from the objectStore with the given filename
            const request = objectStore.get(filename);

            // Success handler
            request.addEventListener("success", () => {
                if (request.result.length == 0) {
                    console.log("No data found");
                    resolve(null);
                }
                resolve(request.result);
            });

            // Error handler
            request.addEventListener("error", () => {
                console.error("An error occurred");
                reject();
            }
            );
        });
    }

    // Download the asset file with the given filename
    public downloadAssetFile(filename: string) {
        return new Promise((resolve, reject) => {
            if (!this.db) {
                throw new Error("Database is not initialized");
            }
            const transaction = this.db.transaction(["edutex_assets"], "readonly");
            const objectStore = transaction.objectStore("edutex_assets");

            // Get the data from the objectStore with the given filename
            const request = objectStore.get(filename);

            // Success handler
            request.addEventListener("success", () => {
                if (request.result.length == 0) {
                    console.log("No data found");
                    resolve(null);
                }
                // Get the data from the objectStore
                let data = request.result.data;
                // download the file
                let blob = new Blob([data], { type: "application/octet-stream" });
                let link = document.createElement("a");
                link.href = window.URL.createObjectURL(blob);
                link.download = filename;
                link.click();
                resolve("File downloaded");
            });

            // Error handler
            request.addEventListener("error", () => {
                console.error("An error occurred");
                reject();
            });
        });
    }

    // Delete the asset file with the given filename
    public deleteAssetFile(filename: string) {
        return new Promise<void>((resolve, reject) => {

            if (!this.db) {
                throw new Error("Database is not initialized");
            }
            const transaction = this.db.transaction(["edutex_assets"], "readwrite");
            const objectStore = transaction.objectStore("edutex_assets");

            // Delete the data from the objectStore with the given filename
            const request = objectStore.delete(filename);

            // Success handler
            request.addEventListener("success", () => {

                resolve();
            });

            // Error handler
            request.addEventListener("error", () => {
                console.error("An error occurred");
                reject();
            });
        });
    }

    // Delete all asset files
    public deleteAssetFiles() {
        return new Promise<void>((resolve, reject) => {

            if (!this.db) {
                throw new Error("Database is not initialized");
            }
            const transaction = this.db.transaction(["edutex_assets"], "readwrite");
            const objectStore = transaction.objectStore("edutex_assets");

            // Delete the data from the objectStore
            const request = objectStore.clear();

            // Success handler
            request.addEventListener("success", () => {

                resolve();
            });

            // Error handler
            request.addEventListener("error", () => {
                console.error("An error occurred");
                reject();
            }
            );
        });
    }

    // Delete the tex files
    public deleteTexFiles() {
        return new Promise<void>((resolve, reject) => {
            if (!this.db) {
                throw new Error("Database is not initialized");
            }
            const transaction = this.db.transaction(["edutex_tex"], "readwrite");

            // Delete the data from the objectStore
            const request = transaction.objectStore("edutex_tex").clear();

            // Success handler
            request.addEventListener("success", () => {

                resolve();
            });

            // Error handler
            request.addEventListener("error", () => {
                console.error("An error occurred");
                reject();
            }
            );
        });
    }

    public addPDFFile(file: File) {
        return Promise.resolve().then(() => {
            // Store the file in the database
            if (!this.db) {
                throw new Error("Database is not initialized");
            }
            const transaction = this.db.transaction(["edutex_pdf"], "readwrite");
            const objectStore = transaction.objectStore("edutex_pdf");

            // Remove the old file
            objectStore.clear();

            // Add the new file
            objectStore.add(file);

            // Success handler
            transaction.addEventListener("complete", () => {
                console.log("Transaction completed");
            });

            // Error handler
            transaction.addEventListener("error", () => {
                console.error("Transaction not opened due to error");
            });
        });
    }

    public getPDFFile() {
        return new Promise((resolve, reject) => {
            if (!this.db) {
                throw new Error("Database is not initialized");
            }
            const transaction = this.db.transaction(["edutex_pdf"], "readonly");
            const objectStore = transaction.objectStore("edutex_pdf");
            const request = objectStore.getAll();

            // Success handler
            request.addEventListener("success", () => {
                if (request.result.length == 0) {
                    console.log("No data found in pdf store");
                    resolve(null);
                }
                resolve(request.result);
            });

            // Error handler
            request.addEventListener("error", () => {
                console.error("An error occurred");
                reject();
            });
        });
    }

    // Delete the pdf file
    public deletePDFFile() {
        return new Promise<void>((resolve, reject) => {
            if (!this.db) {
                throw new Error("Database is not initialized");
            }
            const transaction = this.db.transaction(["edutex_pdf"], "readwrite");
            const objectStore = transaction.objectStore("edutex_pdf");

            // Delete the data from the objectStore
            const request = objectStore.clear();

            // Success handler
            request.addEventListener("success", () => {

                resolve();
            });

            // Error handler
            request.addEventListener("error", () => {
                console.error("An error occurred");
                reject();
            });
        });
    }

    // Clear the database
    public clearDatabase(name: string) {
        return new Promise<void>((resolve, reject) => {

            //    Delete the database
            const deleteRequest = window.indexedDB.deleteDatabase("edutex_db_" + name);

            // Success handler
            deleteRequest.addEventListener("success", () => {
                console.log("Database deleted successfully");
                resolve();
            });

            // Error handler
            deleteRequest.addEventListener("error", () => {
                console.error("Database not deleted due to error");
                reject();
            });

            // Abort handler
            deleteRequest.addEventListener("blocked", () => {
                console.error("Database not deleted due to the operation being blocked");
                reject();
            });
        });
    }

    // Get usage and quota of the indexedDB
    public getIndexedDBQuota() {
        return new Promise((resolve, reject) => {
            navigator.storage.estimate().then((estimate) => {
                resolve({ usage: estimate.usage, quota: estimate.quota });
            });
        });
    }
}
