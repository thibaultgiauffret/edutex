"use strict";

export enum EngineStatus {
  Init = 1,
  Ready = 2,
  Busy = 3,
  Error = 4
}

const ENGINE_PATH: string = 'swiftlatexpdftex.js';

export class CompileResult {
  public pdf: Uint8Array | undefined;
  public status: number = -254;
  public log: string = 'No log';
}

export class PdfTeXEngine {
  private latexWorker: Worker | undefined;
  private latexWorkerStatus: EngineStatus = EngineStatus.Init;

  public async loadEngine(): Promise<void> {
    if (this.latexWorker !== undefined) {
      throw new Error('Other instance is running, abort()');
    }
    this.latexWorkerStatus = EngineStatus.Init;
    await new Promise<void>((resolve, reject) => {
      this.latexWorker = new Worker(ENGINE_PATH);
      this.latexWorker.onmessage = (ev) => {
        const data = ev.data;
        const cmd = data.result;
        if (cmd === 'ok') {
          this.latexWorkerStatus = EngineStatus.Ready;
          resolve();
        } else {
          this.latexWorkerStatus = EngineStatus.Error;
          reject();
        }
      };
    });
    //@ts-ignore
    this.latexWorker.onmessage = (_) => {};
    //@ts-ignore
    this.latexWorker.onerror = (_) => {};
  }

  public isReady(): boolean {
    return this.latexWorkerStatus === EngineStatus.Ready;
  }

  public checkEngineStatus(): void {
    if (!this.isReady()) {
      throw Error('Engine is still spinning or not ready yet!');
    }
  }

  public async compileLaTeX(): Promise<CompileResult> {
    this.checkEngineStatus();
    this.latexWorkerStatus = EngineStatus.Busy;
    const start_compile_time = performance.now();
    const res = await new Promise<CompileResult>((resolve, _) => {
        //@ts-ignore
      this.latexWorker.onmessage = (ev) => {
        const data = ev.data;
        const cmd = data.cmd;
        if (cmd !== "compile") return;
        const result = data.result;
        const log = data.log;
        const status = data.status;
        this.latexWorkerStatus = EngineStatus.Ready;
        console.log('Engine compilation finish ' + (performance.now() - start_compile_time));
        const nice_report = new CompileResult();
        nice_report.status = status;
        nice_report.log = log;
        if (result === 'ok') {
          const pdf = new Uint8Array(data.pdf);
          nice_report.pdf = pdf;
        }
        resolve(nice_report);
      };
      //@ts-ignore
      this.latexWorker.postMessage({ 'cmd': 'compilelatex' });
      console.log('Engine compilation start');
    });
    //@ts-ignore
    this.latexWorker.onmessage = (_) => {};
    return res;
  }

  /* Internal Use */
  public async compileFormat(): Promise<void> {
    this.checkEngineStatus();
    this.latexWorkerStatus = EngineStatus.Busy;
    await new Promise<void>((resolve, reject) => {
        //@ts-ignore
      this.latexWorker.onmessage = (ev) => {
        const data = ev.data;
        const cmd = data.cmd;
        if (cmd !== "compile") return;
        const result = data.result;
        const log = data.log;
        // const status: number = data['status'] as number;
        this.latexWorkerStatus = EngineStatus.Ready;
        if (result === 'ok') {
          const formatArray = data.pdf; /* PDF for result */
          const formatBlob = new Blob([formatArray], { type: 'application/octet-stream' });
          const formatURL = URL.createObjectURL(formatBlob);
          setTimeout(() => { URL.revokeObjectURL(formatURL); }, 30000);
          console.log('Download format file via ' + formatURL);
          resolve();
        } else {
          reject(log);
        }
      };
      //@ts-ignore
      this.latexWorker.postMessage({ 'cmd': 'compileformat' });
    });
    //@ts-ignore
    this.latexWorker.onmessage = (_) => {};
  }

  public setEngineMainFile(filename: string): void {
    this.checkEngineStatus();
    if (this.latexWorker !== undefined) {
      this.latexWorker.postMessage({ 'cmd': 'setmainfile', 'url': filename });
    }
  }

  public writeMemFSFile(filename: string, srccode: string): void {
    this.checkEngineStatus();
    if (this.latexWorker !== undefined) {
      this.latexWorker.postMessage({ 'cmd': 'writefile', 'url': filename, 'src': srccode });
    }
  }

  public makeMemFSFolder(folder: string): void {
    this.checkEngineStatus();
    if (this.latexWorker !== undefined) {
      if (folder === '' || folder === '/') {
        return;
      }
      this.latexWorker.postMessage({ 'cmd': 'mkdir', 'url': folder });
    }
  }

  public flushCache(): void {
    this.checkEngineStatus();
    if (this.latexWorker !== undefined) {
      // console.warn('Flushing');
      this.latexWorker.postMessage({ 'cmd': 'flushcache' });
    }
  }

  public setTexliveEndpoint(url: string): void {
    if (this.latexWorker !== undefined) {
      this.latexWorker.postMessage({ 'cmd': 'settexliveurl', 'url': url });
      this.latexWorker = undefined;
    }
  }

  public closeWorker(): void {
    if (this.latexWorker !== undefined) {
      this.latexWorker.postMessage({ 'cmd': 'grace' });
      this.latexWorker = undefined;
    }
  }
}