import Editor from "./editor";
import "./projects";
import { db, projects } from "./main";
import { PdfTeXEngine } from "./PdfTeXEngine";
import { XeTeXEngine } from "./XeTeXEngine";
import { DvipdfmxEngine } from "./DvipdfmxEngine";
import { default_workspace, simplified_workspace } from "./workspaces";
import _WYSIWYGEditor from "./simple_editor";

// Import Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';
import * as bootstrap from 'bootstrap'

// Import Font Awesome
import '@fortawesome/fontawesome-free/css/all.min.css';

// Import JSZip
import JSZip from 'jszip'

// Import Custom CSS
import "../css/style.css";

// Import LaTeX presets
import { table_preset, figure_preset } from "./latex_presets";

export default class GUI {
  private _db: any;
  private _editors: any;
  private _simpleEditor: any;
  private _projects: any;
  private _engine: string;
  private _fresh: string;
  private _from: string;
  private _mode: string;
  private _autosave: string;
  private _autosaveInterval: undefined | ReturnType<typeof setTimeout>;
  private _theme: string;
  private _workspace: string;
  private _XeTeXEngine: any;
  private _DvipdfmxEngine: any;
  private _globalEn: any;
  private _dvipdfmxEn: any;
  private _historyModal: any;
  private _filesModal: any;
  private _resultsModal: any;
  private _toastEl: HTMLDivElement;
  private _toast: any;
  private _timestamp: number;
  private _editorContent: string;
  private _simpleEditorContent: string;
  private _actualFile: string;
  private _assets: string;
  private _projectName: string;

  public constructor(pageProperties: any) {

    this._db = db;
    this._projects = projects;
    this._editors = {};
    this._simpleEditor = undefined;
    this._engine = pageProperties["engine"];
    this._fresh = pageProperties["fresh"];
    this._from = pageProperties["from"];
    this._mode = pageProperties["mode"];
    this._assets = pageProperties["assets"];
    this._autosave = pageProperties["autosave"];
    this._autosaveInterval = undefined;
    this._theme = pageProperties["theme"];
    this._workspace = pageProperties["workspace"];
    this._XeTeXEngine = XeTeXEngine;
    this._DvipdfmxEngine = DvipdfmxEngine;
    this._toastEl = document.getElementById("toast") as HTMLDivElement;
    this._toast = new bootstrap.Toast(this._toastEl);
    this._filesModal = new bootstrap.Modal('#filesModal', {});
    this._historyModal = new bootstrap.Modal('#historyModal', {});
    this._resultsModal = undefined;

    this._timestamp = 0;
    this._editorContent = "";
    this._simpleEditorContent = "";
    this._actualFile = "main.tex";
    this._projectName = "";
  }

  // Initialize the GUI
  public init() {
    // Check if the document is ready
    document.addEventListener("DOMContentLoaded", () => {

      this.loadWorkspace().then(() => {

        this._editors = { "main.tex": new Editor("main.tex") };

        if (this._workspace == "simplified") {
          // Open the sandbox project
          this._projects.open("simplified");
          this._projectName = "simplified";
          // Check if a fresh load is requested
          this.freshLoad("simplified").then(() => {
            this.startEditor("simplified");
          });
        } else {
          // Check if the mode is widget or sandbox
          if (this._mode != "widget" && this._mode != "sandbox" && this._mode != "workspace") {
            // Remove the widget toolbar
            let widgetToolbar = document.getElementById("widgetToolbar") as HTMLDivElement;
            // Remove the widget toolbar
            widgetToolbar.remove();
            // Show the loader
            this._projects.load();
          } else if (this._mode == "sandbox") {
            // Open the sandbox project
            this._projects.open("sandbox");
            this._projectName = "sandbox";
            // Check if a fresh load is requested
            this.freshLoad("sandbox").then(() => {
              this.startEditor("sandbox");
            });
          } else if (this._mode == "widget") {
            // Set the widget mode and open the sandbox project
            this.setWidgetMode();
            this._projects.open("sandbox");
            // Check if a fresh load is requested
            this.freshLoad("sandbox").then(() => {
              this.startEditor("sandbox");
            });
          }
        }

        // Setup the UI
        this.setupUI();
      }).catch(() => {
        console.log("Workspace not found");
      })
    });
  }

  // Load workspace
  public loadWorkspace() {
    return new Promise<void>((resolve, reject) => {
      // Set the workspace
      console.log("Workspace : " + this._workspace);
      let workspace = document.getElementById("workspace") as HTMLDivElement;
      if (this._workspace == "simplified") {
        this._mode = "simplified"
        workspace.innerHTML = simplified_workspace;
        this._simpleEditor = new _WYSIWYGEditor;
        this._simpleEditor.init().then(() => {
          // Hide the editor-card-footer
          let editorCardFooter = document.getElementById("editor-card-footer");
          editorCardFooter?.classList.add('d-none')
          // Init the results modal
          this._resultsModal = new bootstrap.Modal('#resultsModal', {});
          // Attach the resultsBtn to modal
          let resultsBtn = document.getElementById("resultsBtn")
          resultsBtn?.addEventListener('click', () => {
            this._resultsModal.show();
          });
          resultsBtn?.classList.add("d-none")
          // Hide all the .showProjectsBtn
          let showProjectsBtn = document.getElementsByClassName("showProjectsBtn") as HTMLCollectionOf<HTMLButtonElement>;
          for (let i = 0; i < showProjectsBtn.length; i++) {
            showProjectsBtn[i].classList.add("d-none");
          }
          // Show the .quitSimplifiedBtn
          let quitSimplifiedBtn = document.getElementById("quitSimplifiedBtn") as HTMLButtonElement;
          quitSimplifiedBtn.classList.remove("d-none");
          // Hide the #projectNameSpan
          let projectNameSpan = document.getElementById("projectNameSpan") as HTMLSpanElement;
          projectNameSpan.classList.add("d-none");
          resolve();
        }).catch((error: any) => {
          console.log("Error while initializing the simple editor : " + error);
          reject();
        })
      } else {
        workspace.innerHTML = default_workspace;
        resolve();
      }
    });
  }

  // If fresh is set, clear the indexedDB. Ask the user for confirmation first.
  private async freshLoad(name: string) {
    return new Promise<void>((resolve, reject) => {
      if (this._fresh == "true") {
        if (confirm("La page demandée veut forcer l'effacement des données de l'éditeur. Êtes-vous sûr de vouloir continuer ?")) {
          this._db.clearDatabase(name).then(() => {
            this.showToast("fas fa-trash", "success", "La base de données a été effacée.");
          }).catch((error: any) => {
            this.showToast("fas fa-exclamation-triangle", "danger", "Une erreur est survenue lors de la suppression de la base de données : " + error);
          });
        } else {
          this.showToast("fas fa-exclamation-triangle", "danger", "La page demandée n'a pas pu effacer les données de l'éditeur.");
        }
      }
      resolve();
    });
  }

  // Set up the UI (buttons, events, etc.)
  private setupUI() {

    /*
    * Init all the tooltips
    */
    let tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]');

    let that = this

    // Get all tooltips
    for (let i = 0; i < tooltipTriggerList.length; i++) {
      let tooltipTriggerEl = tooltipTriggerList[i]

      // Check if the device is touch enabled
      if (that.is_touch_enabled()) {
        // Set the tooltip to manual and automatically hide it after 1 second
        tooltipTriggerEl?.addEventListener('click', function () {
          bootstrap.Tooltip.getInstance(tooltipTriggerEl)?.show();
          setTimeout(function () {
            bootstrap.Tooltip.getInstance(tooltipTriggerEl)?.hide();
          }, 1000);
        })

        new bootstrap.Tooltip(tooltipTriggerEl, {
          container: 'body',
          trigger: 'manual'
        });
      } else {
        // Set the tooltip to hover
        new bootstrap.Tooltip(tooltipTriggerEl, {
          container: 'body',
          trigger: 'hover'
        });
      }
    }

    /*
    * Setup the project events
    */
    let projectAddBtn = document.getElementById("projectAddBtn") as HTMLButtonElement;
    projectAddBtn.addEventListener("click", () => {
      // Add the project
      this._projectName = this._projects.add();
      if (this._projectName != "error") {
        this.startEditor(this._projectName);
      } 
      
    });

    let showProjectsBtn = document.getElementsByClassName("showProjectsBtn") as HTMLCollectionOf<HTMLButtonElement>;
    for (let i = 0; i < showProjectsBtn.length; i++) {
      showProjectsBtn[i].addEventListener("click", () => {
        this.saveToDatabase();
        // Refresh the page without parameters
        window.location.href = window.location.href.split("?")[0];
      });
    }

    // Attach event to the openProjectBtn and the deleteProjectBtn
    this.attachProjectEvents();

    /*
    * Setup the navbar button events
    */

    let filesBtn = document.getElementsByClassName("filesBtn") as HTMLCollectionOf<HTMLButtonElement>;
    const filesModal = new bootstrap.Modal('#filesModal', {
    })
    for (let i = 0; i < filesBtn.length; i++) {
      filesBtn[i].addEventListener("click", () => {
        filesModal.show();
      });
    }

    let settingsBtn = document.getElementsByClassName("settingsBtn") as HTMLCollectionOf<HTMLButtonElement>;
    const settingsModal = new bootstrap.Modal('#settingsModal', {
    })
    for (let i = 0; i < settingsBtn.length; i++) {
      settingsBtn[i].addEventListener("click", () => {
        settingsModal.show();
      });
    }

    let aboutBtn = document.getElementsByClassName("aboutBtn") as HTMLCollectionOf<HTMLButtonElement>;
    const aboutModal = new bootstrap.Modal('#aboutModal', {
    })
    for (let i = 0; i < aboutBtn.length; i++) {
      aboutBtn[i].addEventListener("click", () => {
        aboutModal.show();
      });
    }

    let themeToggleBtn = document.getElementsByClassName("themeToggleBtn") as HTMLCollectionOf<HTMLButtonElement>;
    for (let i = 0; i < themeToggleBtn.length; i++) {
      themeToggleBtn[i].addEventListener("click", () => {
        this.toggleTheme();
      });
    }

    /* 
    * Setup settings modal events
    */

    // Attach event to the engine select
    let engineSelect = document.getElementById("engine") as HTMLSelectElement;
    engineSelect.addEventListener("change", () => {
      this.changeEngine(engineSelect.value);
    });

    // Attach event to the autosave input
    let autosaveInput = document.getElementById("autosave") as HTMLInputElement;
    autosaveInput.addEventListener("change", () => {
      this.changeAutosave(parseInt(autosaveInput.value));
    });

    // Update the params
    let engine = document.getElementById("engine") as HTMLInputElement;
    if (engine != null) {
      engine.value = this._engine;
    }
    let autosave = document.getElementById("autosave") as HTMLInputElement;
    if (autosave != null) {
      autosave.value = this._autosave;
    }

    /*
    * Setup the editor button events
    */

    // Attach event to the compile button
    let compileBtn = document.getElementById("compileBtn") as HTMLButtonElement;
    compileBtn.addEventListener("click", () => {
      this.compile();
    });

    // Attach event to the downloadTeXBtn
    let downloadTeXBtn = document.getElementById("downloadTeXBtn") as HTMLButtonElement;
    downloadTeXBtn.addEventListener("click", () => {
      this.downloadTeX();
    });

    // Attach event to the saveBtn
    let saveBtn = document.getElementById("saveBtn") as HTMLButtonElement;
    saveBtn.addEventListener("click", () => {
      this.saveToDatabase();
    });

    // Attach event to the clearBtn
    let clearBtn = document.getElementById("clearBtn") as HTMLButtonElement;
    clearBtn.addEventListener("click", () => {
      this.clearEditor();
    });

    // Attach event to the fileSelect
    let fileSelect = document.getElementById("fileSelect") as HTMLSelectElement;
    fileSelect.addEventListener("change", () => {
      this.loadFile();
    });

    // Attach event to the tablePresetBtn
    let addTableBtn = document.getElementById("addTableBtn") as HTMLButtonElement;
    addTableBtn.addEventListener("click", () => {
      this._editors[this._actualFile].insert(table_preset);
    });

    // Attach event to the figurePresetBtn
    let addFigureBtn = document.getElementById("addFigureBtn") as HTMLButtonElement;
    addFigureBtn.addEventListener("click", () => {
      this._editors[this._actualFile].insert(figure_preset);
    });

    /*
    * Setup the files modal events
    */

    // Attach event to the downloadPDFBtn
    let downloadPDFBtn = document.getElementById("downloadPDFBtn") as HTMLButtonElement;
    downloadPDFBtn.addEventListener("click", () => {
      this.downloadPDF();
    });

    // Attach event to the addFileBtn
    let addFileBtn = document.getElementById("addFileBtn") as HTMLButtonElement;
    addFileBtn.addEventListener("click", () => {
      this.addFile();
    });


    let downloadAllBtn = document.getElementById("downloadAllBtn") as HTMLButtonElement;
    downloadAllBtn.addEventListener("click", () => {
      this.downloadAll();
    });

    let deleteAllBtn = document.getElementById("deleteAllBtn") as HTMLButtonElement;
    deleteAllBtn.addEventListener("click", () => {
      this.deleteAll();
    });

    /*
    * Initialize the dropdowns
    */

    // Init the bootstrap dropdowns
    let dropdownToggle = document.getElementsByClassName("dropdown-toggle") as HTMLCollectionOf<HTMLButtonElement>;
    for (let i = 0; i < dropdownToggle.length; i++) {
      new bootstrap.Dropdown(dropdownToggle[i]);
    }

  }

  // Check if the device is touch enabled
  private is_touch_enabled() {
    return ('ontouchstart' in window) ||
      (navigator.maxTouchPoints > 0)
  }

  private attachProjectEvents() {
    let projectAlert = document.getElementById("projectAlert") as HTMLDivElement;
    // Attach the delete event to the project delete buttons
    let deleteButtons = document.getElementsByClassName("projectDeleteBtn") as HTMLCollectionOf<HTMLButtonElement>;
    Array.from(deleteButtons).forEach((deleteButton: HTMLButtonElement) => {
      let project = deleteButton.getAttribute("data-project-name") as string;
      deleteButton.addEventListener("click", () => {
        if (confirm("Êtes-vous sûr de vouloir supprimer ce projet ?")) {
          // Delete the database
          this._db.deleteDatabase(project).then(() => {
            // Remove the project name from the projectNames array
            let projectNames = localStorage.getItem("projectNames");
            let projects = JSON.parse(projectNames || "[]");
            // Find the index of the object with the name project
            let index = projects.findIndex((obj: any) => obj.name == project);
            // Remove the object
            projects.splice(index, 1);
            // Store the projectNames array in the local storage
            localStorage.setItem("projectNames", JSON.stringify(projects));
            // Load the projects
            this._projects.load();
            this.attachProjectEvents();
            // Show the alert
            projectAlert.classList.remove("d-none");
            projectAlert.innerHTML = "<i class='fa-solid fa-fw fa-check'></i>&nbsp;Le projet a été supprimé.";
          }).catch((error: any) => {
            projectAlert.classList.remove("d-none");
            projectAlert.innerHTML = "<i class='fa-solid fa-fw fa-exclamation-triangle'></i>&nbsp;Une erreur est survenue lors de la suppression du projet : " + error;
          });
        }
      });
    });

    // Attach the open event to the project open buttons
    let openButtons = document.getElementsByClassName("projectOpenBtn") as HTMLCollectionOf<HTMLButtonElement>;
    Array.from(openButtons).forEach((openButton: HTMLButtonElement) => {
      openButton.addEventListener("click", () => {
        this._projectName = openButton.getAttribute("data-project-name") as string;
        this._projects.open(this._projectName);
        this.startEditor(this._projectName);
      });
    });
  }

  // Setup the editor
  private async startEditor(projectName: string) {

    console.log("Starting editor for project " + projectName);

    // Initialize the database
    await this._db.init(projectName);

    // Initialize the editors
    this._editors = { "main.tex": new Editor("main.tex") };
    this._editors["main.tex"].init();
    this._editors["main.tex"].show();

    // If the workspace is simplified, set the editor in read-only mode
    if (this._workspace == "simplified") {
      this._editors["main.tex"].setReadOnly(true);
      // Catch the click event on the editor
      let editor = document.getElementById("main.tex") as HTMLDivElement;
      editor.addEventListener("click", () => {
        this.showToast("fas fa-exclamation-triangle", "danger", "Vous ne pouvez pas modifier le code LaTeX dans ce mode.<br><button class='btn btn-secondary btn-sm mt-2' id='switchToCodeBtn'><i class='fa-solid fa-fw fa-code'></i>&nbsp;Passer en mode standard</button>");
      });
    }

    // Initialize the engine
    if (this._engine == "xelatex") {
      console.log("Using XeLaTeX engine")
      console.log("XeTeXEngine.js loaded");
      this._globalEn = new this._XeTeXEngine();
      this._dvipdfmxEn = new this._DvipdfmxEngine();
      this.initEngine();
    } else {
      console.log("Using pdfLaTeX engine")
      console.log("PdfTeXEngine.js loaded");
      this._globalEn = new PdfTeXEngine;
      this.initEngine();
    }

    // Choose the content to display
    let mainTexText = "";
    let simpleEditorContent = "";
    this._timestamp = new Date().getTime();
    if (this._from != null) {
      // If simplified mode, load the JSON file from the URL
      if (this._workspace == "simplified") {
        const response = await fetch(this._from);
        if (!response.ok) {
          throw new Error(`${response.status} ${response.statusText}`);
        }
        simpleEditorContent = await response.text();
        // Set the content of the simple editor
        this._simpleEditor.setEditorContent(JSON.parse(simpleEditorContent));
      } else {
        // If not simplified mode, load the tex file from the URL
        console.log("Loading ressources from " + this._from);
        // Get the content of the file
        try {
          const response = await fetch(this._from);
          if (!response.ok) {
            throw new Error(`${response.status} ${response.statusText}`);
          }
          mainTexText = await response.text();
          this._editors["main.tex"].setValue(mainTexText);
        } catch (error) {
          console.log('Request failed', error)
          this.showToast("fas fa-exclamation-triangle", "danger", "Une erreur est survenue lors de la récupération du fichier. Veuillez vérifier l'URL.");
        }
      }
    } else if (this._fresh == "true") {
      // If the fresh parameter is set, load the empty editor
      console.log("Loading empty editor");
      mainTexText = "";
      this._editors["main.tex"].setValue(mainTexText);
      if (this._workspace == "simplified") {
        // Set the content of the simple editor
        this._simpleEditor.empty()
      }
    } else {
      let editorContents = await this._db.getLastEditorContent();
      if (editorContents != null) {
        // If the fresh parameter is not set, load the editor content from the database
        this._editorContent = editorContents.content;
        console.log("Editor content found in database : " + this._editorContent);
        // Check if the editor content exists
        console.log("Editor content found in database");
        mainTexText = this._editorContent;
        this._editors["main.tex"].setValue(mainTexText);
        this.showToast("fas fa-upload", "success", "Une sauvegarde automatique a été restaurée (" + new Date(this._timestamp).toLocaleString() + ").");
      } else {
        // Load the default main.tex file
        console.log("Loading main.tex file");
        mainTexText = await fetch("./examples/main.tex").then(response => response.text());
        this._editors["main.tex"].setValue(mainTexText);
      }

      // Load the simple editor content from the database
      if (this._workspace == "simplified") {
        if (editorContents != null) {
          this._simpleEditorContent = editorContents.simpleContent;
          console.log("Simple editor content found in database");
          console.log(this._simpleEditorContent)
          this._simpleEditor.setEditorContent(this._simpleEditorContent);
        } else {
          // If no simple editor content is found, empty the simple editor and the main editor
          this._simpleEditor.empty()
        }
      }
    }

    // Check if the assets parameter is set
    if (this._assets != null) {
      // Split the assets parameter
      let assetsArray = this._assets.split(",");
      let max = assetsArray.length;
      let i = 0;
      // For each asset, get the content of the file
      assetsArray.forEach(async (asset) => {
        // Get the content of the file
        try {
          const response = await fetch(asset);
          if (!response.ok) {
            throw new Error(`${response.status} ${response.statusText}`);
          }
          let data = await response.arrayBuffer();
          // Convert data to blob
          data = new Uint8Array(data);
          let fileData = {
            ["name"]: asset.split("/").pop(),
            type: "text/plain",
            size: data.byteLength,
            data: data
          }
          this._db.addAssetData(fileData, true).then((data: string) => {
            i++;
            this.showToast("fas fa-upload", "success", "Le fichier " + fileData.name + " a été ajouté.");
            if (i == max) {
              // Update the file list after all files have been added
              this.updateFileList();
            }
          }).catch((error: any) => {
            i++;
            this.showToast("fas fa-exclamation-triangle", "danger", "Une erreur est survenue lors de l'ajout du fichier dans la base de données : " + error);
            if (i == max) {
              // Update the file list after all files have been added (or not...)
              this.updateFileList();
            }
          });
        } catch (error) {
          console.log('Request failed', error)
          this.showToast("fas fa-exclamation-triangle", "danger", "Une erreur est survenue lors de la récupération du fichier. Veuillez vérifier l'URL.");
        }
      });
    } else {
      // Update the file list
      this.updateFileList(true);
    }

    // Update the theme
    if (this._theme === "light") {
      this.setLightTheme();
    } else {
      this.setDarkTheme();
    }

    // Update the settings modal
    let engineSelect = document.getElementById("engine") as HTMLSelectElement;
    engineSelect.value = this._engine;
    let autosave = document.getElementById("autosave") as HTMLInputElement;
    autosave.value = this._autosave;

    // Set the autosave interval
    this._autosaveInterval = setInterval(() => {
      this.saveToDatabase();
    }, parseInt(this._autosave) * 1000);

    // Empty the history modal
    let historyList = document.getElementById("historyList") as HTMLDivElement;
    historyList.innerHTML = "";

    // Update the compile button
    let compileBtn = document.getElementById("compileBtn") as HTMLButtonElement;
    compileBtn.innerHTML = "<i class='fa-solid fa-fw fa-play'></i>";
    compileBtn.disabled = false;
    let consoleOutput = document.getElementById("console") as HTMLDivElement;
    consoleOutput.innerHTML = this.consoleInitMessage;

    // Init the history content and the pdf
    this._db.getLastTenEditorContent().then((lastTenEntries: any) => {
      if (lastTenEntries == null || lastTenEntries == undefined) {
        this._editorContent = ""
        this._timestamp = new Date().getTime();
        let li = document.createElement("li");
        li.className = "dropdown-item";
        li.setAttribute("id", "noHistory");
        li.innerHTML = "Aucune sauvegarde trouvée.";
        let historyList = document.getElementById("historyList") as HTMLDivElement;
        historyList.appendChild(li);
      } else {
        // Add the history entries to the history list
        console.log("Last 10 entries found. Adding them to the history list.");
        lastTenEntries.forEach((entry: any) => {
          this.addHistoryEntry(entry.timestamp);
        });
        this._editorContent = lastTenEntries[lastTenEntries.length - 1].content;
        this._timestamp = lastTenEntries[lastTenEntries.length - 1].timestamp;
      }
      this._db.getPDFFile().then((pdf: any) => {
        if (pdf != null) {
          console.log("PDF found in database. Displaying it.");
          const objectURL = URL.createObjectURL(pdf[0].file);
          // Display the PDF
          let pdfbox = document.getElementById("pdfbox") as HTMLDivElement;
          pdfbox.innerHTML = `<embed src="${objectURL}" width="100%" height="100%" type="application/pdf">`;
          setTimeout(() => {
            URL.revokeObjectURL(objectURL);
          }, 30000);
          // Show the pdf download button
          let pdfDownloadBtn = document.getElementById("downloadPDFBtn");
          pdfDownloadBtn?.classList.remove("d-none");

          // Show the resultsModal button
          if (this._workspace == "simplified") {
            let resultsBtn = document.getElementById("resultsBtn")
            resultsBtn?.classList.remove("d-none")
          }
        } else {
          // Display the pdf placeholder
          let pdfbox = document.getElementById("pdfbox") as HTMLDivElement;
          pdfbox.innerHTML = this.pdfPlaceholder;
          // Hide the timer element
          let compileTime = document.getElementById("compile-time") as HTMLDivElement;
          compileTime.classList.add("d-none");
          // Hide the pdf download button
          let pdfDownloadBtn = document.getElementById("downloadPDFBtn");
          pdfDownloadBtn?.classList.add("d-none");
        }
      }).catch((error: any) => {
        this.showToast("fas fa-exclamation-triangle", "danger", "Une erreur est survenue lors de la récupération du PDF : " + error);
      });
    }).catch((error: any) => {
      this.showToast("fas fa-exclamation-triangle", "danger", "Une erreur est survenue lors de la récupération des sauvegardes : " + error);
    });
    // Hide the loader
    this.hideLoader();
  }

  // Initialize the engine
  private async initEngine() {
    console.log("Initializing engine");

    await this._globalEn.loadEngine();
    if (this._engine == "xelatex") {
      await this._dvipdfmxEn.loadEngine();
    }
  }

  // Compile the document with the chosen engine and update the UI.
  private async compile() {
    // Save the editor content in database
    this.saveToDatabase(false);
    // Choose the engine
    if (localStorage.getItem("engine") == "xelatex") {
      if (!this._globalEn.isReady() || !this._dvipdfmxEn.isReady()) {
        console.log("Engine not ready yet");
        return;
      }
    } else {
      if (!this._globalEn.isReady()) {
        console.log("Engine not ready yet");
        return;
      }
    }

    // Update the compile button
    let compileBtn = document.getElementById("compileBtn") as HTMLButtonElement;
    compileBtn.disabled = true;
    compileBtn.innerHTML = "<i class='fa-solid fa-fw fa-spinner fa-spin'></i>";

    // Update the console
    let compileInitMessage = "<strong>>> Compilation lancée à " + new Date().toLocaleTimeString() + "</strong><br>";

    // Start the timer
    let startTime = new Date().getTime();

    // Prepare the compilation
    let downloadReq = await fetch('examples/edutex.png');
    let imageBlob = await downloadReq.arrayBuffer();
    this._globalEn.writeMemFSFile("main.tex", this._editorContent);
    this._globalEn.writeMemFSFile("edutex.png", new Uint8Array(imageBlob));

    // For each file in the assets object store, add it to the memfs
    let files = await this._db.getAssetsData();
    if (files != null) {
      files.forEach((file: any) => {
        this._globalEn.writeMemFSFile(file.name, new Uint8Array(file.data));
      });
    }

    // Set the main file
    this._globalEn.setEngineMainFile("main.tex");

    // Compile the document
    let r = await this._globalEn.compileLaTeX();

    // Find latex errors in r.log and change the color of the line
    let lines = r.log.split("\n");
    let newLog = "";
    lines.forEach((line: string) => {
      if (line.startsWith("!")) {
        newLog = newLog + "<span class='text-danger'>" + line + "</span><br>";
      } // Check if the line begins with "l."
      else if (line.startsWith("l.")) {
        // Get the line number after "l."
        let lineNumber = line.split("l.")[1].split(" ")[0];
        newLog = newLog + "<span class='text-danger'>" + line + "</span><span class='float-end'><button class='btn btn-sm btn-danger errorBtn' data-line-number='" + lineNumber + "'><i class='fa-solid fa-fw fa-eye'></i></button></span>";
      } else {
        newLog = newLog + line + "<br>";
      }
    });

    // Update the console
    let consoleOutput = document.getElementById("console") as HTMLDivElement;
    consoleOutput.innerHTML = consoleOutput.innerHTML + "\n" + compileInitMessage + newLog;
    compileBtn.innerHTML = "<i class='fa-solid fa-fw fa-play'></i>";
    compileBtn.disabled = false;

    // Check if the compilation was successful
    if (r.status === 0) {
      let pdfblob;
      if (this._engine == "xelatex") {
        // Write the xdv file in the memfs
        this._dvipdfmxEn.writeMemFSFile("main.xdv", r.pdf);
        this._dvipdfmxEn.setEngineMainFile("main.xdv");
        let r1 = await this._dvipdfmxEn.compilePDF();
        pdfblob = new Blob([r1.pdf], { type: 'application/pdf' });
      } else {
        pdfblob = new Blob([r.pdf], { type: 'application/pdf' });
      }

      // Create a new object URL for the pdf
      const objectURL = URL.createObjectURL(pdfblob);

      // Display the PDF
      let pdfbox = document.getElementById("pdfbox") as HTMLDivElement;
      pdfbox.innerHTML = `<embed src="${objectURL}" width="100%" height="100%" type="application/pdf">`;
      // Show the pdf download button
      let pdfDownloadBtn = document.getElementById("downloadPDFBtn");
      pdfDownloadBtn?.classList.remove("d-none");
      setTimeout(() => {
        URL.revokeObjectURL(objectURL);
      }, 30000);

      // Save the PDF in the indexedDB
      this._db.addPDFFile({ file: pdfblob }).then(() => {
        console.log("PDF saved");
        // Update the file list
        this.updateFileList(true);
      }).catch((error: any) => {
        this.showToast("fas fa-exclamation-triangle", "danger", "Une erreur est survenue lors de la sauvegarde du PDF dans la base de données : " + error);
      });
    } else {
      // Attach the rename event to errorBtn
      let errorBtns = document.getElementsByClassName("errorBtn") as HTMLCollectionOf<HTMLButtonElement>;
      Array.from(errorBtns).forEach((errorBtn: HTMLButtonElement) => {
        errorBtn.addEventListener("click", () => {
          this.seeLine(parseInt(errorBtn.getAttribute("data-line-number") as string));
        });
      });
      this.showToast("fas fa-exclamation-triangle", "danger", "La compilation a échoué. Veuillez vérifier la console.");
    }

    // Scroll the console
    consoleOutput.scrollTop = consoleOutput.scrollHeight;

    // Stop the timer
    let endTime = new Date().getTime();
    let timeDiff = endTime - startTime;
    // Calculate minutes and seconds
    let minutes = Math.floor(timeDiff / 60000);
    let seconds = ((timeDiff % 60000) / 1000).toFixed(0);
    // Display the time
    let compileTime = document.getElementById("compile-time") as HTMLDivElement;
    compileTime.innerHTML = "<i class='fa-solid fa-fw fa-hourglass-half'></i>&nbsp;" + minutes + " m " + seconds + " s";

    // Display the time element
    compileTime.classList.remove("d-none");

    // If workspace is simplified, show the modal
    if (this._workspace == "simplified") {
      let resultsBtn = document.getElementById("resultsBtn")
      resultsBtn?.classList.remove("d-none")
      this._resultsModal.show()
    }
  }

  // Highlight a line in the editor
  private seeLine(lineNumber: number) {
    console.log("Seeing line " + lineNumber);
    this._editors["main.tex"].goToLine(lineNumber);
    this._editors["main.tex"].focus();
  }

  // Download the content of the ace editor as a .tex file
  private async downloadTeX() {
    let text = this._editors[this._actualFile].getValue();
    let blob = new Blob([text], { type: "text/plain;charset=utf-8" });
    this.saveAs(blob, "main.tex");
  }

  // Download pdf
  private async downloadPDF() {
    let pdf = await this._db.getPDFFile();
    if (pdf[0].file != null) {
      this.saveAs(pdf[0].file, "main.pdf");
    } else {
      this.showToast("fas fa-exclamation-triangle", "danger", "Le fichier PDF n'a pas pu être téléchargé.");
    }
  }

  // Clear the editor
  private clearEditor() {
    if (confirm("Êtes-vous sûr de vouloir vider l'éditeur ?")) {
      // Clear the editor
      this._editors[this._actualFile].setValue("");
      // If workspace is simplified, clear the simple editor
      if (this._workspace == "simplified") {
        this._simpleEditor.empty()
      }
      // Delete the PDF from the database
      this._db.deletePDFFile().then(() => {
        // Clear the console
        let consoleOutput = document.getElementById("console") as HTMLDivElement;
        consoleOutput.innerHTML = this.consoleInitMessage;
        let pdfbox = document.getElementById("pdfbox") as HTMLDivElement;
        // Display the pdf placeholder
        pdfbox.innerHTML = this.pdfPlaceholder;
        // Update the file list
        this.updateFileList(true);
        this.showToast("fas fa-trash", "success", "L'éditeur a été vidé.");
      }).catch((error: any) => {
        this.showToast("fas fa-exclamation-triangle", "danger", "Une erreur est survenue lors de la suppression du PDF : " + error);
      })
    }

  }

  // Save file function
  private saveAs(blob: Blob, filename: string) {
    let link = document.createElement('a');
    link.href = window.URL.createObjectURL(blob);
    link.download = filename;
    link.click();
  }

  // Update the file list (in file modal and in fileSelect)
  private updateFileList(noload = false) {
    console.log("Updating file list");
    // Clear the file list
    let fileList = document.getElementById("fileList") as HTMLTableElement;
    fileList.innerHTML = "";
    // Add the main.tex file
    let tr = document.createElement("tr");
    // Add the file icon
    let td0 = document.createElement("td");
    td0.innerHTML = "<i class='fa-solid fa-fw fa-file-alt'></i>";
    tr.appendChild(td0);
    // Add the file name
    let td1 = document.createElement("td");
    td1.innerHTML = "main.tex";
    tr.appendChild(td1);
    // file type
    let td2 = document.createElement("td");
    td2.innerHTML = this.displaySize(this._editors["main.tex"].getValue().length);
    tr.appendChild(td2);
    // Add the download button
    let td3 = document.createElement("td");
    let downloadBtn = document.createElement("button");
    downloadBtn.className = "btn btn-sm btn-primary mx-2";
    downloadBtn.innerHTML = "<i class='fa-solid fa-fw fa-download'></i>";
    downloadBtn.addEventListener("click", () => {
      this.downloadTeX();
    });
    td3.appendChild(downloadBtn);
    tr.appendChild(td3);

    // Add the main.tex file to the fileSelect
    let fileSelect = document.getElementById("fileSelect") as HTMLSelectElement;
    fileSelect.innerHTML = "";
    let option = document.createElement("option");
    option.value = "main.tex";
    option.innerHTML = "main.tex";
    fileSelect.appendChild(option);

    // Add the table row to the table body
    fileList.appendChild(tr);
    // Get the files from the assets object store
    this._db.getAssetsData().then((files: any) => {
      if (files != null) {

        // Add the files to the file list
        files.forEach((file: any) => {
          // Add a table row
          let tr = document.createElement("tr");
          // Add the file icon
          let td0 = document.createElement("td");
          td0.innerHTML = this.chooseIcon(file.name);
          tr.appendChild(td0);
          // Add the file name
          let td1 = document.createElement("td");
          td1.innerHTML = file.name;
          tr.appendChild(td1);
          // File type
          let td2 = document.createElement("td");
          td2.innerHTML = this.displaySize(file.size);
          tr.appendChild(td2);
          // Add the download button
          let td3 = document.createElement("td");
          let downloadBtn = document.createElement("button");
          downloadBtn.className = "btn btn-sm btn-primary mx-2";
          downloadBtn.innerHTML = "<i class='fa-solid fa-fw fa-download'></i>";
          downloadBtn.addEventListener("click", () => {
            this._db.downloadAssetFile(file.name);
          });
          td3.appendChild(downloadBtn);
          let deleteBtn = document.createElement("button");
          deleteBtn.className = "btn btn-sm btn-danger";
          deleteBtn.innerHTML = "<i class='fa-solid fa-fw fa-trash'></i>";
          deleteBtn.addEventListener("click", () => {
            this.deleteFile(file.name);
          });
          td3.appendChild(deleteBtn);
          tr.appendChild(td3);
          fileList.appendChild(tr);

          // Add tex and sty file to fileSelect
          if (file.name.endsWith(".tex") || file.name.endsWith(".sty")) {
            let option = document.createElement("option");
            option.value = file.name;
            option.innerHTML = file.name;
            fileSelect.appendChild(option);

            // Create a new editor for the file
            if (file.name != "main.tex") {
              this._editors[file.name] = new Editor(file.name);
              this._editors[file.name].init();
            }

            // Select the file in the fileSelect
            if (file.name == this._actualFile) {
              fileSelect.value = file.name;
            }

            if (noload) {
              return;
            }
            this.loadFile();
          }


        });
      }

      // Add the pdf file to the file list
      this._db.getPDFFile().then((pdf: any) => {
        if (pdf != null) {
          // Add a table row
          let tr = document.createElement("tr");
          let td0 = document.createElement("td");
          td0.innerHTML = "<i class='fa-solid fa-fw fa-file-pdf'></i>";
          tr.appendChild(td0);
          // Add the file name
          let td1 = document.createElement("td");
          td1.innerHTML = "main.pdf";
          tr.appendChild(td1);
          // file type
          let td2 = document.createElement("td");
          td2.innerHTML = this.displaySize(pdf[0].file.size);
          tr.appendChild(td2);
          // Add the download button
          let td3 = document.createElement("td");
          let downloadBtn = document.createElement("button");
          downloadBtn.className = "btn btn-sm btn-primary mx-2";
          downloadBtn.innerHTML = "<i class='fa-solid fa-fw fa-download'></i>";
          downloadBtn.setAttribute("onclick", "downloadPDF();");
          td3.appendChild(downloadBtn);
          let deleteBtn = document.createElement("button");
          deleteBtn.className = "btn btn-sm btn-danger";
          deleteBtn.innerHTML = "<i class='fa-solid fa-fw fa-trash'></i>";
          deleteBtn.addEventListener("click", () => {
            this.deletePDF();
          });
          td3.appendChild(deleteBtn);
          tr.appendChild(td3);
          // Add the table row to the table body
          fileList.appendChild(tr);
        }
      }).catch((error: any) => {
        this.showToast("fas fa-exclamation-triangle", "danger", "Une erreur est survenue lors de la récupération du PDF : " + error);
      });

      // Get the quota and usage. Update the progress bar storageProgress
      this._db.getIndexedDBQuota().then((data: any) => {
        let quota = data.quota;
        let usage = data.usage;
        let percentage = usage / quota * 100;
        let storageProgressValue = document.getElementById("storageProgressValue") as HTMLDivElement;
        storageProgressValue.innerHTML = Math.round(percentage) + "%";
        storageProgressValue.setAttribute("aria-valuenow", percentage.toString());
        storageProgressValue.style.width = percentage + "%";
        let storageValues = document.getElementById("storageValues") as HTMLDivElement;
        storageValues.innerHTML = Math.round(usage / 1E6 * 100) / 100 + " Mo / " + Math.round(quota / 1E6 * 100) / 100 + " Mo (" + Math.round(percentage) + "%)";
      }).catch((error: any) => {
        this.showToast("fas fa-exclamation-triangle", "danger", "Une erreur est survenue lors de la récupération de l'espace de stockage : " + error);
      })

    }).catch((error: any) => {
      this.showToast("fas fa-exclamation-triangle", "danger", "Une erreur est survenue lors de la récupération des fichiers : " + error);
    });
  }

  // Display the file size in bytes
  private displaySize(bytes: number) {
    if (bytes < 1000) {
      return bytes + " o";
    } else if (bytes < 1E6) {
      return Math.round(bytes / 1E3 * 100) / 100 + " ko";
    } else {
      return Math.round(bytes / 1E6 * 100) / 100 + " Mo";
    }
  }

  // Choose the icon according to the file extension
  private chooseIcon(filename: string) {
    if (filename.endsWith(".tex")) {
      return "<i class='fa-solid fa-fw fa-file-alt'></i>";
    } else if (filename.endsWith(".sty")) {
      return "<i class='fa-solid fa-fw fa-file-code'></i>";
    } else if (filename.endsWith(".png") || filename.endsWith(".jpg") || filename.endsWith(".jpeg") || filename.endsWith(".gif") || filename.endsWith(".svg")) {
      return "<i class='fa-solid fa-fw fa-file-image'></i>";
    } else if (filename.endsWith(".pdf")) {
      return "<i class='fa-solid fa-fw fa-file-pdf'></i>";
    } else if (filename.endsWith(".zip") || filename.endsWith(".rar") || filename.endsWith(".7z")) {
      return "<i class='fa-solid fa-fw fa-file-archive'></i>";
    } else if (filename.endsWith(".txt")) {
      return "<i class='fa-solid fa-fw fa-file-alt'></i>";
    } else if (filename.endsWith(".bib")) {
      return "<i class='fa-solid fa-fw fa-file-alt'></i>";
    } else if (filename.endsWith(".cls")) {
      return "<i class='fa-solid fa-fw fa-file-code'></i>";
    } else {
      return "<i class='fa-solid fa-fw fa-file'></i>";
    }
  }

  // Set the widget mode
  private setWidgetMode() {
    // Remove class d-none from the widget toolbar
    let widgetToolbar = document.getElementById("widgetToolbar") as HTMLDivElement;
    widgetToolbar.classList.remove("d-none");
    // Hide the navbar
    let head = document.getElementById("head") as HTMLDivElement;
    head.classList.add("d-none");
    // Hide the footer
    let foot = document.getElementById("foot") as HTMLDivElement;
    foot.classList.add("d-none");
    // Find all elements with class sidebyside
    let sidebyside = document.getElementsByClassName("sidebyside") as HTMLCollectionOf<HTMLElement>;
    // Replace the class sidebyside with sidebyside-widget
    Array.from(sidebyside).forEach((element: HTMLElement) => {
      element.classList.remove("sidebyside");
      element.classList.add("sidebyside-widget");
    })
    // Find all elements with class full_height
    let full_height = document.getElementsByClassName("full_height") as HTMLCollectionOf<HTMLElement>;
    // Replace the class full_height with full_height-widget
    Array.from(full_height).forEach((element: HTMLElement) => {
      element.classList.remove("full_height");
      element.classList.add("full_height-widget");
    })
    // Make the body background transparent
    document.getElementsByTagName("body")[0].style.backgroundColor = "transparent";
  }

  // Hide the loader
  private hideLoader() {
    // Wait 0.5 second before hiding the loader (bad solution)
    setTimeout(() => {
      let loader = document.getElementById("loader") as HTMLDivElement;
      loader.classList.add("hidden");
    }, 500);
  }

  private showToast(icon: string, type: string, message: string) {
    console.log("Showing toast with message : " + message)

    // Set the toast icon
    let toast_header_icon = this._toastEl.querySelector("#toast-header i");
    if (toast_header_icon != null) {
      toast_header_icon.className = icon;
    }

    // Set the toast type
    let toast_header = this._toastEl.querySelector("#toast-header")
    if (toast_header != null) {
      toast_header.className = "toast-header bg-" + type + " text-white";
    }

    // Set the toast message
    let toast_body = this._toastEl.querySelector(".toast-body");
    if (toast_body != null) {
      toast_body.innerHTML = message;
    }

    // Show the toast
    this._toast.show();
    let hr = "";
    // Check if document.getElementById("notifList").childElementCount > 0
    let notifList = document.getElementById("notifList") as HTMLDivElement;
    if (notifList.childElementCount > 0) {
      // Add a hr
      hr = "<hr class='dropdown-divider'>"
    }
    // Add the toast to the notifList
    let li = document.createElement("li");
    li.className = "list-group-item";
    li.innerHTML = '<div class="row mx-2"><div class="col-2" style="text-align:center;"><i class="' + icon + ' text-' + type + '"></i></div><div class="col-10">' + message + '<br><span class="text-secondary">' + new Date().toLocaleTimeString() + '</span></div></div>' + hr;
    notifList.prepend(li);
    // If there are more than 10 entries in the notifList, remove the last one
    if (notifList.childElementCount > 20 && notifList.lastChild != null) {
      notifList.lastChild.remove();
    }

  }

  // Save the editor content in indexedDB
  private saveToDatabase(notify = true) {
    console.log("Saving " + this._actualFile + " to database");
    // If the file is not main.tex, save the file in the assets object store
    if (this._actualFile != "main.tex") {
      let data = this._editors[this._actualFile].getValue();
      // Convert data to Uint8Array
      let encoder = new TextEncoder();
      data = encoder.encode(data);
      // Save the file in the assets object store
      let fileData = {
        ["name"]: this._actualFile,
        type: "text/plain",
        size: data.length,
        data: data
      }
      this._db.updateAssetData(fileData).then(() => {
        if (notify) {
          this.showToast("fas fa-upload", "success", "Le fichier " + this._actualFile + " a été sauvegardé.");
        }
      }).catch((error: any) => {
        this.showToast("fas fa-exclamation-triangle", "danger", "Une erreur est survenue lors de la sauvegarde du fichier dans la base de données : " + error);
      });
      return;
    }

    // If the history placeholder is present, remove it
    let noHistory = document.getElementById("noHistory") as HTMLDivElement;
    if (noHistory != null) {
      noHistory.remove();
    }
    this._timestamp = new Date().getTime();

    // Get the last editor content from the database
    this._db.getLastEditorContent().then((last: any) => {
      // Check if the editor content has changed since the last save
      if (last != null) {
        // If the editor content has not changed since the last save, do nothing
        if (last.content == this._editors["main.tex"].getValue()) {
          console.log("Editor content has not changed since the last save. Update the timestamp of the last entry.");
          this._editorContent = this._editors["main.tex"].getValue();
          // Replace the timestamp of the last entry
          this.updateHistoryEntry(this._timestamp);
          // Remove the last entry
          this._db.removeLastEditorContent();
          // Save the editor content in the indexedDB
          if (this._workspace == "simplified") {
            this._simpleEditor.save().then((data: string) => {
              this._db.saveEditorContent(this._timestamp, this._editorContent, data);
            })
          } else {
            this._db.saveEditorContent(this._timestamp, this._editorContent);
          }
          return;
        } else {
          // Update the history list
          this.addHistoryEntry(this._timestamp);
        }
      } else {
        // Add the history entry to the history list
        this.addHistoryEntry(this._timestamp);
      }
      this._editorContent = this._editors["main.tex"].getValue();
      // Save the editor content in the indexedDB
      if (this._workspace == "simplified") {
        this._simpleEditor.save().then((data: any) => {
          this._db.saveEditorContent(this._timestamp, this._editorContent, data);
        })
      } else {
        this._db.saveEditorContent(this._timestamp, this._editorContent);
      }
    }).catch((error: any) => {
      this.showToast("fas fa-exclamation-triangle", "danger", "Une erreur est survenue lors de la sauvegarde du contenu de l'éditeur dans la base de données : " + error);
    })

    if (!notify) {
      return;
    }
    this.showToast("fas fa-save", "success", "Votre travail a été sauvegardé dans le navigateur.");
  }

  // Load a file in the editor (when updateFileList is called)
  private loadFile() {
    // Save the editor content in database
    this.saveToDatabase(false);
    let fileSelect = document.getElementById("fileSelect") as HTMLSelectElement;
    this._actualFile = fileSelect.value;
    if (this._actualFile == "main.tex") {
      // Get the keys of the editors object
      let keys = Object.keys(this._editors);
      // Hide all editors
      for (let editor of keys) {
        this._editors[editor].hide();
      }
      // Show the historyBtn
      let historyBtn = document.getElementById("historyBtn") as HTMLButtonElement;
      historyBtn.classList.remove("d-none");
      // Show the main editor
      this._editors["main.tex"].show();
    } else {
      // Hide the historyBtn
      let historyBtn = document.getElementById("historyBtn") as HTMLButtonElement;
      historyBtn.classList.add("d-none");
      this._db.getAssetData(this._actualFile).then((data: any) => {
        // Convert the Uint8Array to a string
        let decoder = new TextDecoder();
        let text = decoder.decode(data.data);
        // Get the keys of the editors object
        let keys = Object.keys(this._editors);
        // Hide all editors
        keys.forEach((key: string) => {
          this._editors[key].hide();
        });
        // Show the editor
        this._editors[this._actualFile].show();
        this._editors[this._actualFile].setValue(text);
      }).catch((error: any) => {
        this.showToast("fas fa-exclamation-triangle", "danger", "Une erreur est survenue lors du chargement du fichier : " + error);
      });
    }
  }

  // Add a file
  private addFile() {
    this.getFileFromInput().then((data: any) => {
      if (data == null) {
        this.showToast("fas fa-exclamation-triangle", "danger", "Veuillez sélectionner un fichier à ajouter !");
        return;
      }
      if (data.name == "main.tex") {
        this._db.addTexFile(data).then((content: string) => {
          console.log(content);
          this._editors["main.tex"].setValue(content);
          this.showToast("fas fa-upload", "success", "Le fichier " + data.name + " a été remplacé.");
        })
        // .catch((error: any) => {
        //   this.showToast("fas fa-exclamation-triangle", "danger", "Une erreur est survenue lors de l'ajout du fichier dans la base de données : " + error);
        // })
      } else {
        this._db.addAssetData(data).then((message: string) => {
          if (message == "replaced") {
            this.showToast("fas fa-upload", "success", "Le fichier " + data.name + " a été remplacé.");
          } else if (message == "added") {
            this.showToast("fas fa-upload", "success", "Le fichier " + data.name + " a été ajouté.");
          } else if (message == "not_added") {
            console.log("File " + data.name + " not added");
          }
          // Update the file list
          this.updateFileList();

        }).catch((error: any) => {
          this.showToast("fas fa-exclamation-triangle", "danger", "Une erreur est survenue lors de l'ajout du fichier dans la base de données : " + error);
        });
      }
    }).catch((error: any) => {
      this.showToast("fas fa-exclamation-triangle", "danger", "Une erreur est survenue lors de la récupération du fichier : " + error);
    });
  }

  // Get a file from the input
  private getFileFromInput = () => {
    return new Promise((resolve, reject) => {
      // Get the file from the input
      let fileInput = document.getElementById('fileInput') as HTMLInputElement;
      console.log(fileInput.files);
      if (fileInput.files == undefined || fileInput.files.length == 0) {
        resolve(null);
      } else {
        // Get the file
        const file = fileInput.files[0];
        // Create a new FileReader
        const reader = new FileReader();
        // When the file is loaded, resolve the promise
        reader.onload = (event) => {
          fileInput.value = '';
          if (event.target == null) {
            reject("No file selected");
          } else {
            // Send the file to the resolve function
            resolve({
              ["name"]: file.name,
              type: file.type,
              size: file.size,
              data: event.target.result,
            });
          }
        };
        reader.onerror = (event) => {
          if (event.target == null) {
            reject("No file selected");
          } else {
            reject(event.target.error);
          }
        };
        // Read the file
        reader.readAsArrayBuffer(file);
      }
    });
  };

  // Delete a file
  private deleteFile(file: string) {
    if (confirm("Êtes-vous sûr de vouloir supprimer ce fichier ?")) {
      // Switch to the main.tex file
      this._actualFile = "main.tex";
      // Change the fileSelect value
      let fileSelect = document.getElementById("fileSelect") as HTMLSelectElement;
      fileSelect.value = "main.tex";
      this.loadFile();
      // Delete the file
      this._db.deleteAssetFile(file).then(() => {
        // Update the file list
        this.updateFileList();
        this.showToast("fas fa-trash", "success", "Le fichier " + file + " a été supprimé.");
      }).catch((error: any) => {
        this.showToast("fas fa-exclamation-triangle", "danger", "Une erreur est survenue lors de la suppression du fichier : " + error);
      });
    }
  }

  // Add a history entry to the history list
  private addHistoryEntry(timestamp: number) {
    console.log("Adding history entry " + timestamp + " to the history list");
    // Create a new list item
    let date = new Date(timestamp);
    let li = document.createElement("li");
    li.className = "dropdown-item";
    li.setAttribute("id", timestamp.toString());
    let span = document.createElement("span");
    span.className = "small";
    span.innerHTML = date.toLocaleString();
    let button1 = document.createElement("button");
    button1.className = "btn btn-sm btn-secondary ms-3";
    let icon1 = document.createElement("i");
    icon1.className = "fas fa-eye";
    button1.appendChild(icon1);
    let button2 = document.createElement("button");
    button2.className = "btn btn-sm btn-primary ms-2";
    let icon2 = document.createElement("i");
    icon2.className = "fas fa-upload";
    button2.appendChild(icon2);
    let button3 = document.createElement("button");
    button3.className = "btn btn-sm btn-danger ms-2";
    let icon3 = document.createElement("i");
    icon3.className = "fas fa-trash";
    button3.appendChild(icon3);
    li.appendChild(span);
    li.appendChild(button1);
    li.appendChild(button2);
    li.appendChild(button3);
    let historyList = document.getElementById("historyList") as HTMLDivElement;
    historyList.prepend(li);

    // Attach the seeHistoryEntry event to button1
    button1.addEventListener("click", () => {
      this.seeHistoryEntry(timestamp);
    });

    // Attach the loadFromHistory event to button2
    button2.addEventListener("click", () => {
      this.loadFromHistory(timestamp);
    });

    // Attach the deleteFromHistory event to button3
    button3.addEventListener("click", () => {
      this.deleteFromHistory(timestamp);
    });

    // If there are more than 10 entries in the history list, remove the last one
    if (historyList.childElementCount > 10 && historyList.lastChild != null) {
      historyList.lastChild.remove();
    }
  }

  // Update the last history entry in the history list
  private updateHistoryEntry(timestamp: number) {
    console.log("Updating history entry " + timestamp + " in the history list");
    let historyList = document.getElementById("historyList") as HTMLDivElement;
    // Remove the last history entry
    if (historyList.firstChild != null) {
      historyList.firstChild.remove();
    }
    // Add the history entry
    this.addHistoryEntry(timestamp);
  }

  // Load a history entry in the editor
  private loadFromHistory(timestamp: number) {
    if (confirm("Êtes-vous sûr de vouloir charger cette sauvegarde ?")) {
      // Get the editor content from the database by timestamp
      this._db.getEditorContentByTimestamp(timestamp).then((data: any) => {
        // Set the editor content
        this._editors["main.tex"].setValue(data.content);
        // If workspace is simplified, set the simple editor content
        if (this._workspace == "simplified") {
          this._simpleEditor.setEditorContent(data.simpleContent)
        }
        // Hide the modal
        this._historyModal.hide();
      }).catch((error: any) => {
        this.showToast("fas fa-exclamation-triangle", "danger", "Une erreur est survenue lors du chargement de la sauvegarde : " + error);
      })
    }
  }

  // Delete a history entry
  private deleteFromHistory(timestamp: number) {
    if (confirm("Êtes-vous sûr de vouloir supprimer cette sauvegarde ?")) {
      console.log("Deleting history entry " + timestamp);
      // Delete the history entry from the database by timestamp
      this._db.removeEditorContentByTimestamp(timestamp).then(() => {
        // Remove the history entry from the history list
        let historyEntry = document.getElementById(timestamp.toString());
        historyEntry?.remove();
        // If there are no more history entries, add a "no history" message
        let historyList = document.getElementById("historyList") as HTMLDivElement;
        if (historyList.childElementCount == 0) {
          let li = document.createElement("li");
          li.className = "dropdown-item";
          li.setAttribute("id", "noHistory");
          li.innerHTML = "Aucune sauvegarde trouvée.";
          historyList.appendChild(li);
        }
      }).catch((error: any) => {
        this.showToast("fas fa-exclamation-triangle", "danger", "Une erreur est survenue lors de la suppression de la sauvegarde : " + error);
      })
    }
  }

  // See a history entry in the modal
  private seeHistoryEntry(timestamp: number) {

    // Show the modal
    this._historyModal.show();

    // Get the editor content from the database by timestamp
    this._db.getEditorContentByTimestamp(timestamp).then((data: any) => {
      // Set the modal title
      let historyModalLabel = document.getElementById("historyModalLabel") as HTMLDivElement;
      historyModalLabel.innerHTML = "Sauvegarde du " + new Date(timestamp).toLocaleString();
      // Set the modal body
      let historyModalBody = document.getElementById("historyModalBody");
      if (historyModalBody != null) {
        historyModalBody.innerHTML = "<pre>" + data.content + "</pre>";
      }
      // Attach the loadFromHistory event to the restoreBtn
      let restoreBtn = document.getElementById("restoreBtn");
      restoreBtn?.addEventListener("click", () => {
        this.loadFromHistory(timestamp);
      });
    }).catch((error: any) => {
      this.showToast("fas fa-exclamation-triangle", "danger", "Une erreur est survenue lors de la récupération du contenu de la sauvegarde : " + error);
    })
  }

  // Initial message in the console
  private consoleInitMessage = `<div class="alert alert-info" role="alert"><b>Bienvenue dans l'éditeur LaTeX d'EnSciences !</b></div><div class="alert alert-warning" role="alert"><b>Note :</b> La première compilation peut durer un certain temps car les packages LaTeX sont téléchargés... Merci pour votre patience !
    </div>`;

  // PDF placeholder
  private pdfPlaceholder = '<div id="pdf-placeholder"><p><i class="fa-solid fa-fw fa-file-pdf fa-4x"></i></p><p>Pas de PDF généré pour l\'instant...</p></div>';

  // Toggle the theme
  private toggleTheme() {
    if (this._editors["main.tex"].getTheme() === "ace/theme/monokai") {
      this.setLightTheme();
    } else {
      this.setDarkTheme();
    }
  }

  // Set the theme to light
  private setLightTheme() {
    // For each editor, set the theme to light
    for (let editor in this._editors) {
      this._editors[editor].setTheme("ace/theme/chrome");
    }
    // Set the html tag "data-bs-theme" to "light" for bootstrap
    let htmlTag = document.getElementsByTagName("html")[0];
    htmlTag.setAttribute("data-bs-theme", "light");
    // Set the theme toggle icon to moon
    let themeToggleBtn = document.getElementById("theme-toggle-icon");
    if (themeToggleBtn != null) {
      themeToggleBtn.innerHTML = "<i class='fa-solid fa-fw fa-moon'></i>";
    }
    // Update the theme in the local storage
    localStorage.setItem("theme", "light");
  }

  // Set the theme to dark
  private setDarkTheme() {
    // For each editor, set the theme to dark
    for (let editor in this._editors) {
      this._editors[editor].setTheme("ace/theme/monokai");
    }
    // Set the html tag "data-bs-theme" to "dark" for bootstrap
    let htmlTag = document.getElementsByTagName("html")[0];
    htmlTag.setAttribute("data-bs-theme", "dark");
    // Set the theme toggle icon to sun
    let themeToggleBtn = document.getElementById("theme-toggle-icon");
    if (themeToggleBtn != null) {
      themeToggleBtn.innerHTML = "<i class='fa-solid fa-fw fa-sun'></i>";
    }
    // Update the theme in the local storage
    localStorage.setItem("theme", "dark");
  }

  // Change the engine
  private changeEngine(value: string) {
    if (confirm("Le changement de moteur LaTeX requiert le rechargement de la page. Êtes-vous sûr de vouloir continuer ?")) {
      // Save the engine in the local storage
      if (value == "xelatex") {
        localStorage.setItem("engine", "xelatex");
      } else {
        localStorage.setItem("engine", "pdflatex");
      }
      // Reload the page after saving the content of the editor
      this.saveToDatabase();
      location.reload();
    }
  }

  // Change the autosave interval
  private changeAutosave(value: number) {
    console.log("Autosave interval changed to " + value + " seconds")
    // Clear the autosave interval
    clearInterval(this._autosaveInterval);
    // Set the new autosave interval
    if (value != 0) {
      this._autosaveInterval = setInterval(() => {
        this.saveToDatabase();
      }, value * 1000);
    }
    value = value * 1000;
    // Save the autosave interval in the local storage
    localStorage.setItem("autosaveInterval", value.toString());
  }

  // Delete the PDF
  private deletePDF() {
    if (confirm("Êtes-vous sûr de vouloir supprimer le PDF ?")) {
      this._db.deletePDFFile().then(() => {
        // Hide the pdf download button
        let pdfDownloadBtn = document.getElementById("downloadPDFBtn");
        pdfDownloadBtn?.classList.add("d-none");
        // Remove the pdf from the pdfbox
        let pdfbox = document.getElementById("pdfbox") as HTMLDivElement;
        pdfbox.innerHTML = this.pdfPlaceholder;
        // Update the file list
        this.updateFileList(true);
        this.showToast("fas fa-trash", "success", "Le PDF a été supprimé.");
      }).catch((error: any) => {
        this.showToast("fas fa-exclamation-triangle", "danger", "Une erreur est survenue lors de la suppression du PDF : " + error);
      });
    }
  }

  // Download all files (main.tex, pdf and assets)
  private async downloadAll() {
    // Prepare the zip file
    let zip = new JSZip();
    // Add the main.tex file
    zip.file("main.tex", this._editors["main.tex"].getValue());
    // Add the pdf file
    let pdf = await this._db.getPDFFile();
    if (pdf != null) {
      zip.file("main.pdf", pdf[0].file);
    }
    // Add the assets
    let files = await this._db.getAssetsData();
    if (files != null) {
      files.forEach((file: any) => {
        zip.file(file.name, new Uint8Array(file.data));
      });
    }
    // Download the zip file
    let that = this;
    zip.generateAsync({ type: "blob" }).then(function (content) {
      that.saveAs(content, that._projectName + "_" + new Date().toLocaleDateString() + ".zip");
      // Close the modal
      that._filesModal.hide();
    });
  }

  // Delete all files (pdf, assets and editor content)
  private deleteAll() {
    if (confirm("Êtes-vous sûr de vouloir supprimer tous les fichiers ?")) {
      // Delete the tex files
      this._db.deleteTexFiles().then(() => {
        // Delete the pdf
        this._db.deletePDFFile().then(() => {
          // Hide the pdf download button
          let pdfDownloadBtn = document.getElementById("downloadPDFBtn");
          if (pdfDownloadBtn != null) {
            pdfDownloadBtn.classList.add("d-none");
          }
          // Delete the assets
          this._db.deleteAssetFiles().then(() => {
            // Clear the history list and add a "no history" message
            let historyList = document.getElementById("historyList") as HTMLDivElement;
            historyList.innerHTML = "";
            let li = document.createElement("li");
            li.className = "dropdown-item";
            li.setAttribute("id", "noHistory");
            li.innerHTML = "Aucune sauvegarde trouvée.";
            historyList.appendChild(li);
            // Update the file list
            this.updateFileList();
            // Clear the editor
            this.clearEditor();
            // Show the toast
            this.showToast("fas fa-trash", "success", "Tous les fichiers ont été supprimés.");
            this._filesModal.hide();
          }).catch((error: any) => {
            this.showToast("fas fa-exclamation-triangle", "danger", "Une erreur est survenue lors de la suppression des fichiers : " + error);
          });
        }).catch((error: any) => {
          this.showToast("fas fa-exclamation-triangle", "danger", "Une erreur est survenue lors de la suppression du PDF : " + error);
        });
      }).catch((error: any) => {
        this.showToast("fas fa-exclamation-triangle", "danger", "Une erreur est survenue lors de la suppression des fichiers : " + error);
      });
    }

  }
}