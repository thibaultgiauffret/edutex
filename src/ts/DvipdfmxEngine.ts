enum EngineStatus {
    Init = 1,
    Ready = 2,
    Busy = 3,
    Error = 4
}

const XDVPDFMX_ENGINE_PATH: string = 'swiftlatexdvipdfm.js';

class CompileResult {
    pdf: Uint8Array | undefined;
    status: number = -254;
    log: string = 'No log';
}

class DvipdfmxEngine {
    latexWorker: Worker | undefined;
    latexWorkerStatus: EngineStatus = EngineStatus.Init;

    async loadEngine(): Promise<void> {
        if (this.latexWorker !== undefined) {
            throw new Error('Other instance is running, abort()');
        }
        this.latexWorkerStatus = EngineStatus.Init;
        await new Promise<void>((resolve, reject) => {
            this.latexWorker = new Worker(XDVPDFMX_ENGINE_PATH);
            this.latexWorker.onmessage = (ev: MessageEvent) => {
                const data = ev.data;
                const cmd = data.result;
                if (cmd === 'ok') {
                    this.latexWorkerStatus = EngineStatus.Ready;
                    resolve();
                } else {
                    this.latexWorkerStatus = EngineStatus.Error;
                    reject();
                }
            };
        });
        // @ts-ignore
        this.latexWorker.onmessage = (_) => { };
        // @ts-ignore
        this.latexWorker.onerror = (_) => { };
    }

    isReady(): boolean {
        return this.latexWorkerStatus === EngineStatus.Ready;
    }

    checkEngineStatus(): void {
        if (!this.isReady()) {
            throw Error('Engine is still spinning or not ready yet!');
        }
    }

    async compilePDF(): Promise<CompileResult> {
        this.checkEngineStatus();
        this.latexWorkerStatus = EngineStatus.Busy;
        const start_compile_time = performance.now();
        const res = await new Promise<CompileResult>((resolve, _) => {
            // @ts-ignore
            this.latexWorker.onmessage = (ev: MessageEvent) => {
                const data = ev.data;
                const cmd = data.cmd;
                if (cmd !== 'compile') return;
                const result = data.result;
                const log = data.log;
                const status = data.status;
                this.latexWorkerStatus = EngineStatus.Ready;
                console.log('Engine compilation finish ' + (performance.now() - start_compile_time));
                const nice_report = new CompileResult();
                nice_report.status = status;
                nice_report.log = log;
                if (result === 'ok') {
                    const pdf = new Uint8Array(data.pdf);
                    nice_report.pdf = pdf;
                }
                resolve(nice_report);
            };
            // @ts-ignore
            this.latexWorker.postMessage({ cmd: 'compilepdf' });
            console.log('Engine compilation start');
        });
        // @ts-ignore
        this.latexWorker.onmessage = (_) => { };
        return res;
    }

    setEngineMainFile(filename: string): void {
        this.checkEngineStatus();
        if (this.latexWorker !== undefined) {
            this.latexWorker.postMessage({ cmd: 'setmainfile', url: filename });
        }
    }

    writeMemFSFile(filename: string, srccode?: Uint8Array | ArrayBuffer): void {
        this.checkEngineStatus();
        if (srccode === undefined) {
            return;
        }
        if (srccode instanceof ArrayBuffer) {
            srccode = new Uint8Array(srccode);
        }
        if (this.latexWorker !== undefined) {
            this.latexWorker.postMessage({ cmd: 'writefile', url: filename, src: srccode });
        }
    }

    makeMemFSFolder(folder: string): void {
        this.checkEngineStatus();
        if (this.latexWorker !== undefined) {
            if (folder === '' || folder === '/') {
                return;
            }
            this.latexWorker.postMessage({ cmd: 'mkdir', url: folder });
        }
    }

    setTexliveEndpoint(url: string): void {
        if (this.latexWorker !== undefined) {
            this.latexWorker.postMessage({ 'cmd': 'settexliveurl', 'url': url });
            this.latexWorker = undefined;
        }
    }

    closeWorker(): void {
        if (this.latexWorker !== undefined) {
            this.latexWorker.postMessage({ cmd: 'grace' });
            this.latexWorker = undefined;
        }
    }
}

export { DvipdfmxEngine, EngineStatus };

